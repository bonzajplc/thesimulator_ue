#pragma once

UENUM()
enum class EMagicaVoxelErrorCodes
{
	NoError = 0,
	FileDoesNotExist = -1,
	NotAMagicaFile = -2,
	WrongVersion = -3,
	ErrorUnknown = -4,
	InvalidMainChunkID = -5
};

UENUM()
enum class EMagicaVoxelLayerType
{
    LayerNone = 0,
    LayerStatic = 1,
    LayerDynamic = 2,
    LayerWater = 3,
    LayerHardSurface = 4
};

