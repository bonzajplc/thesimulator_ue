// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "VoxelModel.h"
#include "MagicaVoxelEnums.h"

namespace BinaryUtils
{
	// Reads 32 bits from Src to Dest if != nullptr.
	// Increments Src pointer by 4 in any case.
	void ReadInt32(int32* Dest, const uint8*& Src);

	void ReadSingle(float* Dest, const uint8*& Src);

	void ReadBytes(uint8* Dest, const uint8*& Src, const int NumBytes);
	void IncrementPointer(const uint8*& Src, const int NumBytes);

	// Return true if data in both arrays is the same, false otherwise
	bool CompareData(const uint8* SourceData, const char* DataToCompare, const int NumBytes);
}

struct MagicaVoxelVoxel
{
public:

	uint8 X, Y, Z, ColorIndex;
};

class MagicaVoxelChunk
{
public:

	TArray<MagicaVoxelVoxel*> Voxels;

	int NumVoxels = 0;
	int SizeX = 0, SizeY = 0, SizeZ = 0;

	TArray<FLinearColor> Palette;
	TArray<FLinearColor> Materials;
	int ModelID = -1;

	static TArray<FLinearColor> DefaultPalette;

	MagicaVoxelChunk(int NewModelID);

	void PaletteToTexture(void) const;
	void MaterialsToTexture(void) const;
};

struct MagicaVoxelHierarchyNode
{
public:

	int32 NodeID = -1;
	int32 ModelID = -1;
	int32 Layer = -1;

	FMatrix TransformMatrix = FMatrix::Identity;
	FMatrix RotationMatrix = FMatrix::Identity;

	TArray<int> ChildrenIDs;
	TArray<MagicaVoxelHierarchyNode*> Children;
};

class MagicaVoxelInstance
{
public:

	MagicaVoxelChunk* VoxelChunk;
	int Layer = -1;

	FMatrix FinalMatrix = FMatrix::Identity;
	FMatrix FinalRotationMatrix = FMatrix::Identity;

	MagicaVoxelInstance() {}
};

/**
 * 
 */
class MAGICAVOXELMODULE_API MagicaVoxelImporter
{
public:

	MagicaVoxelImporter() = delete;

	static EMagicaVoxelErrorCodes LoadVOX(
		const FString& RelativePath,
		const bool bCreateInstances,
		TArray<MagicaVoxelInstance*>& OutInstances,
		TArray<VoxelModel>& OutVoxelModels
	);

	static EMagicaVoxelErrorCodes LoadVOXFromData(
		const TArray64<uint8>& Data,
		const bool bCreateInstances,
		const FString& RelativePath,
		TArray<MagicaVoxelInstance*>& OutInstances,
		TArray<VoxelModel>& OutVoxelModels
	);

private:

	static const int MaxSupportedVersion = 150;

	static int ReadSizeChunk(const uint8*& DataPtr, MagicaVoxelChunk* const Chunk);
	static int ReadVoxelChunk(const uint8*& DataPtr, MagicaVoxelChunk* const Chunk);
	static int ReadPalette(const uint8*& DataPtr, FLinearColor* const OutColors);
	static int ReadMaterialMATT(const uint8*& DataPtr, FLinearColor* const OutMaterials);
	static int ReadMaterialMATL(const uint8*& DataPtr, FLinearColor* const OutMaterials);
	static int ReadLAYR(const uint8*& DataPtr, TMap<int, EMagicaVoxelLayerType>& OutLayers);
	static int ReadHierarchyTransform(const uint8*& DataPtr, TArray<MagicaVoxelHierarchyNode*>& OutHierarchy);
	static int ReadHierarchyGroup(const uint8*& DataPtr, TArray<MagicaVoxelHierarchyNode*>& OutHierarchy);
	static int ReadHierarchyShape(const uint8*& DataPtr, TArray<MagicaVoxelHierarchyNode*>& OutHierarchy);

	static MagicaVoxelHierarchyNode ResolveHierarchy(
		TArray<MagicaVoxelHierarchyNode*>& VOXHierarchyFlat,
		const TArray<MagicaVoxelChunk*>& Models);

	static void CreateInstances(
		const MagicaVoxelHierarchyNode& Node,
		const FMatrix Matrix,
		const FMatrix RotationMatrix,
		const TArray<MagicaVoxelChunk*>& VoxelModels,
		TArray<MagicaVoxelInstance*>& OutInstances);

	static void CreateVoxelModels(
		const TArray<MagicaVoxelInstance*> Instances,
		const FString RelativePath,
		const TMap<int, EMagicaVoxelLayerType> Layers,
		TArray<VoxelModel>& OutVoxelModels);

	static int ReadString(const uint8*& DataPtr, FString& OutString);
	static int ReadDictionary(const uint8*& DataPtr, TMap<FString, FString>& OutDictionary);
};
