#pragma once

#include "CoreMinimal.h"

class FMagicaVoxelModule : public IModuleInterface
{
	public:

		virtual void StartupModule() override;
		virtual void ShutdownModule() override;
};