// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class MAGICAVOXELMODULE_API MagicaVoxelManager
{
public:
	static constexpr float WorldVoxelScale = 0.25f;

	MagicaVoxelManager();
	~MagicaVoxelManager();
};
