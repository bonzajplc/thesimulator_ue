// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "VoxelModelBase.h"
#include "MagicaVoxelEnums.h"

/**
 * 
 */
class MAGICAVOXELMODULE_API VoxelModel : public VoxelModelBase
{
public:
	VoxelModel();
	~VoxelModel();

	static VoxelModel CreateVOXModel(
		const FString& Name,
		const FString& Path,
		const int InstanceID,
		const EMagicaVoxelLayerType LayerType,
		const float Density,
		const FVector LocalPosition,
		const FQuat LocalRotation);
};
