// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "SimulatedObject.h"
#include "MagicaVoxelEnums.h"

/**
 * 
 */
class MAGICAVOXELMODULE_API VoxelModelBase
{
public:

	FString FilePath = "";
	int InstanceID = 0;

	ESimulatedObjectState SimulatedObjectState = ESimulatedObjectState::Frozen;
	EMagicaVoxelLayerType LayerType = EMagicaVoxelLayerType::LayerNone;
    EDestroyUnitsType DestroyUnits = EDestroyUnitsType::All;

	float Density = 3.0f;

	int SubdivisionX = 32;
	int SubdivisionY = 32;
	int SubdivisionZ = 32;

	bool WakeOthers = false;

	FVector TransformPosition = FVector::ZeroVector;
	FQuat TransformRotation = FQuat::Identity;
	FVector TransformScale = FVector::OneVector;

	VoxelModelBase();
	~VoxelModelBase();
};
