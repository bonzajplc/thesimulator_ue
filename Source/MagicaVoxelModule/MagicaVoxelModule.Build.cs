using UnrealBuildTool;
using System.IO;

public class MagicaVoxelModule : ModuleRules
{
    public MagicaVoxelModule(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "CoreUObject",
            "Engine",
            "RHI",
            "Renderer"
        });

        //string EnginePath = Path.GetFullPath(Target.RelativeEnginePath);
        //string SrcRuntimePath = EnginePath + "Source/Runtime/";

        //PublicIncludePaths.AddRange(new string[] {"RenderingModule/Public"});
        //PrivateIncludePaths.AddRange(new string[] { SrcRuntimePath + "Renderer/Private"});
    }
}