#include "../Public/MagicaVoxelModule.h"


void FMagicaVoxelModule::StartupModule()
{
	UE_LOG(LogTemp, Log, TEXT("MagicaVoxel module has started!"));
}

void FMagicaVoxelModule::ShutdownModule()
{
	UE_LOG(LogTemp, Log, TEXT("MagicaVoxel module has shutdown!"));
}


IMPLEMENT_MODULE(FMagicaVoxelModule, MagicaVoxelModule)