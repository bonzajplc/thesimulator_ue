// Fill out your copyright notice in the Description page of Project Settings.


#include "MagicaVoxelImporter.h"
#include "MagicaVoxelManager.h"
#include "VoxelModel.h"

TArray<FLinearColor> MagicaVoxelChunk::DefaultPalette = {
	FLinearColor(1.000000f, 1.000000f, 1.000000f),
	FLinearColor(1.000000f, 1.000000f, 0.800000f),
	FLinearColor(1.000000f, 1.000000f, 0.600000f),
	FLinearColor(1.000000f, 1.000000f, 0.400000f),
	FLinearColor(1.000000f, 1.000000f, 0.200000f),
	FLinearColor(1.000000f, 1.000000f, 0.000000f),
	FLinearColor(1.000000f, 0.800000f, 1.000000f),
	FLinearColor(1.000000f, 0.800000f, 0.800000f),
	FLinearColor(1.000000f, 0.800000f, 0.600000f),
	FLinearColor(1.000000f, 0.800000f, 0.400000f),
	FLinearColor(1.000000f, 0.800000f, 0.200000f),
	FLinearColor(1.000000f, 0.800000f, 0.000000f),
	FLinearColor(1.000000f, 0.600000f, 1.000000f),
	FLinearColor(1.000000f, 0.600000f, 0.800000f),
	FLinearColor(1.000000f, 0.600000f, 0.600000f),
	FLinearColor(1.000000f, 0.600000f, 0.400000f),
	FLinearColor(1.000000f, 0.600000f, 0.200000f),
	FLinearColor(1.000000f, 0.600000f, 0.000000f),
	FLinearColor(1.000000f, 0.400000f, 1.000000f),
	FLinearColor(1.000000f, 0.400000f, 0.800000f),
	FLinearColor(1.000000f, 0.400000f, 0.600000f),
	FLinearColor(1.000000f, 0.400000f, 0.400000f),
	FLinearColor(1.000000f, 0.400000f, 0.200000f),
	FLinearColor(1.000000f, 0.400000f, 0.000000f),
	FLinearColor(1.000000f, 0.200000f, 1.000000f),
	FLinearColor(1.000000f, 0.200000f, 0.800000f),
	FLinearColor(1.000000f, 0.200000f, 0.600000f),
	FLinearColor(1.000000f, 0.200000f, 0.400000f),
	FLinearColor(1.000000f, 0.200000f, 0.200000f),
	FLinearColor(1.000000f, 0.200000f, 0.000000f),
	FLinearColor(1.000000f, 0.000000f, 1.000000f),
	FLinearColor(1.000000f, 0.000000f, 0.800000f),
	FLinearColor(1.000000f, 0.000000f, 0.600000f),
	FLinearColor(1.000000f, 0.000000f, 0.400000f),
	FLinearColor(1.000000f, 0.000000f, 0.200000f),
	FLinearColor(1.000000f, 0.000000f, 0.000000f),
	FLinearColor(0.800000f, 1.000000f, 1.000000f),
	FLinearColor(0.800000f, 1.000000f, 0.800000f),
	FLinearColor(0.800000f, 1.000000f, 0.600000f),
	FLinearColor(0.800000f, 1.000000f, 0.400000f),
	FLinearColor(0.800000f, 1.000000f, 0.200000f),
	FLinearColor(0.800000f, 1.000000f, 0.000000f),
	FLinearColor(0.800000f, 0.800000f, 1.000000f),
	FLinearColor(0.800000f, 0.800000f, 0.800000f),
	FLinearColor(0.800000f, 0.800000f, 0.600000f),
	FLinearColor(0.800000f, 0.800000f, 0.400000f),
	FLinearColor(0.800000f, 0.800000f, 0.200000f),
	FLinearColor(0.800000f, 0.800000f, 0.000000f),
	FLinearColor(0.800000f, 0.600000f, 1.000000f),
	FLinearColor(0.800000f, 0.600000f, 0.800000f),
	FLinearColor(0.800000f, 0.600000f, 0.600000f),
	FLinearColor(0.800000f, 0.600000f, 0.400000f),
	FLinearColor(0.800000f, 0.600000f, 0.200000f),
	FLinearColor(0.800000f, 0.600000f, 0.000000f),
	FLinearColor(0.800000f, 0.400000f, 1.000000f),
	FLinearColor(0.800000f, 0.400000f, 0.800000f),
	FLinearColor(0.800000f, 0.400000f, 0.600000f),
	FLinearColor(0.800000f, 0.400000f, 0.400000f),
	FLinearColor(0.800000f, 0.400000f, 0.200000f),
	FLinearColor(0.800000f, 0.400000f, 0.000000f),
	FLinearColor(0.800000f, 0.200000f, 1.000000f),
	FLinearColor(0.800000f, 0.200000f, 0.800000f),
	FLinearColor(0.800000f, 0.200000f, 0.600000f),
	FLinearColor(0.800000f, 0.200000f, 0.400000f),
	FLinearColor(0.800000f, 0.200000f, 0.200000f),
	FLinearColor(0.800000f, 0.200000f, 0.000000f),
	FLinearColor(0.800000f, 0.000000f, 1.000000f),
	FLinearColor(0.800000f, 0.000000f, 0.800000f),
	FLinearColor(0.800000f, 0.000000f, 0.600000f),
	FLinearColor(0.800000f, 0.000000f, 0.400000f),
	FLinearColor(0.800000f, 0.000000f, 0.200000f),
	FLinearColor(0.800000f, 0.000000f, 0.000000f),
	FLinearColor(0.600000f, 1.000000f, 1.000000f),
	FLinearColor(0.600000f, 1.000000f, 0.800000f),
	FLinearColor(0.600000f, 1.000000f, 0.600000f),
	FLinearColor(0.600000f, 1.000000f, 0.400000f),
	FLinearColor(0.600000f, 1.000000f, 0.200000f),
	FLinearColor(0.600000f, 1.000000f, 0.000000f),
	FLinearColor(0.600000f, 0.800000f, 1.000000f),
	FLinearColor(0.600000f, 0.800000f, 0.800000f),
	FLinearColor(0.600000f, 0.800000f, 0.600000f),
	FLinearColor(0.600000f, 0.800000f, 0.400000f),
	FLinearColor(0.600000f, 0.800000f, 0.200000f),
	FLinearColor(0.600000f, 0.800000f, 0.000000f),
	FLinearColor(0.600000f, 0.600000f, 1.000000f),
	FLinearColor(0.600000f, 0.600000f, 0.800000f),
	FLinearColor(0.600000f, 0.600000f, 0.600000f),
	FLinearColor(0.600000f, 0.600000f, 0.400000f),
	FLinearColor(0.600000f, 0.600000f, 0.200000f),
	FLinearColor(0.600000f, 0.600000f, 0.000000f),
	FLinearColor(0.600000f, 0.400000f, 1.000000f),
	FLinearColor(0.600000f, 0.400000f, 0.800000f),
	FLinearColor(0.600000f, 0.400000f, 0.600000f),
	FLinearColor(0.600000f, 0.400000f, 0.400000f),
	FLinearColor(0.600000f, 0.400000f, 0.200000f),
	FLinearColor(0.600000f, 0.400000f, 0.000000f),
	FLinearColor(0.600000f, 0.200000f, 1.000000f),
	FLinearColor(0.600000f, 0.200000f, 0.800000f),
	FLinearColor(0.600000f, 0.200000f, 0.600000f),
	FLinearColor(0.600000f, 0.200000f, 0.400000f),
	FLinearColor(0.600000f, 0.200000f, 0.200000f),
	FLinearColor(0.600000f, 0.200000f, 0.000000f),
	FLinearColor(0.600000f, 0.000000f, 1.000000f),
	FLinearColor(0.600000f, 0.000000f, 0.800000f),
	FLinearColor(0.600000f, 0.000000f, 0.600000f),
	FLinearColor(0.600000f, 0.000000f, 0.400000f),
	FLinearColor(0.600000f, 0.000000f, 0.200000f),
	FLinearColor(0.600000f, 0.000000f, 0.000000f),
	FLinearColor(0.400000f, 1.000000f, 1.000000f),
	FLinearColor(0.400000f, 1.000000f, 0.800000f),
	FLinearColor(0.400000f, 1.000000f, 0.600000f),
	FLinearColor(0.400000f, 1.000000f, 0.400000f),
	FLinearColor(0.400000f, 1.000000f, 0.200000f),
	FLinearColor(0.400000f, 1.000000f, 0.000000f),
	FLinearColor(0.400000f, 0.800000f, 1.000000f),
	FLinearColor(0.400000f, 0.800000f, 0.800000f),
	FLinearColor(0.400000f, 0.800000f, 0.600000f),
	FLinearColor(0.400000f, 0.800000f, 0.400000f),
	FLinearColor(0.400000f, 0.800000f, 0.200000f),
	FLinearColor(0.400000f, 0.800000f, 0.000000f),
	FLinearColor(0.400000f, 0.600000f, 1.000000f),
	FLinearColor(0.400000f, 0.600000f, 0.800000f),
	FLinearColor(0.400000f, 0.600000f, 0.600000f),
	FLinearColor(0.400000f, 0.600000f, 0.400000f),
	FLinearColor(0.400000f, 0.600000f, 0.200000f),
	FLinearColor(0.400000f, 0.600000f, 0.000000f),
	FLinearColor(0.400000f, 0.400000f, 1.000000f),
	FLinearColor(0.400000f, 0.400000f, 0.800000f),
	FLinearColor(0.400000f, 0.400000f, 0.600000f),
	FLinearColor(0.400000f, 0.400000f, 0.400000f),
	FLinearColor(0.400000f, 0.400000f, 0.200000f),
	FLinearColor(0.400000f, 0.400000f, 0.000000f),
	FLinearColor(0.400000f, 0.200000f, 1.000000f),
	FLinearColor(0.400000f, 0.200000f, 0.800000f),
	FLinearColor(0.400000f, 0.200000f, 0.600000f),
	FLinearColor(0.400000f, 0.200000f, 0.400000f),
	FLinearColor(0.400000f, 0.200000f, 0.200000f),
	FLinearColor(0.400000f, 0.200000f, 0.000000f),
	FLinearColor(0.400000f, 0.000000f, 1.000000f),
	FLinearColor(0.400000f, 0.000000f, 0.800000f),
	FLinearColor(0.400000f, 0.000000f, 0.600000f),
	FLinearColor(0.400000f, 0.000000f, 0.400000f),
	FLinearColor(0.400000f, 0.000000f, 0.200000f),
	FLinearColor(0.400000f, 0.000000f, 0.000000f),
	FLinearColor(0.200000f, 1.000000f, 1.000000f),
	FLinearColor(0.200000f, 1.000000f, 0.800000f),
	FLinearColor(0.200000f, 1.000000f, 0.600000f),
	FLinearColor(0.200000f, 1.000000f, 0.400000f),
	FLinearColor(0.200000f, 1.000000f, 0.200000f),
	FLinearColor(0.200000f, 1.000000f, 0.000000f),
	FLinearColor(0.200000f, 0.800000f, 1.000000f),
	FLinearColor(0.200000f, 0.800000f, 0.800000f),
	FLinearColor(0.200000f, 0.800000f, 0.600000f),
	FLinearColor(0.200000f, 0.800000f, 0.400000f),
	FLinearColor(0.200000f, 0.800000f, 0.200000f),
	FLinearColor(0.200000f, 0.800000f, 0.000000f),
	FLinearColor(0.200000f, 0.600000f, 1.000000f),
	FLinearColor(0.200000f, 0.600000f, 0.800000f),
	FLinearColor(0.200000f, 0.600000f, 0.600000f),
	FLinearColor(0.200000f, 0.600000f, 0.400000f),
	FLinearColor(0.200000f, 0.600000f, 0.200000f),
	FLinearColor(0.200000f, 0.600000f, 0.000000f),
	FLinearColor(0.200000f, 0.400000f, 1.000000f),
	FLinearColor(0.200000f, 0.400000f, 0.800000f),
	FLinearColor(0.200000f, 0.400000f, 0.600000f),
	FLinearColor(0.200000f, 0.400000f, 0.400000f),
	FLinearColor(0.200000f, 0.400000f, 0.200000f),
	FLinearColor(0.200000f, 0.400000f, 0.000000f),
	FLinearColor(0.200000f, 0.200000f, 1.000000f),
	FLinearColor(0.200000f, 0.200000f, 0.800000f),
	FLinearColor(0.200000f, 0.200000f, 0.600000f),
	FLinearColor(0.200000f, 0.200000f, 0.400000f),
	FLinearColor(0.200000f, 0.200000f, 0.200000f),
	FLinearColor(0.200000f, 0.200000f, 0.000000f),
	FLinearColor(0.200000f, 0.000000f, 1.000000f),
	FLinearColor(0.200000f, 0.000000f, 0.800000f),
	FLinearColor(0.200000f, 0.000000f, 0.600000f),
	FLinearColor(0.200000f, 0.000000f, 0.400000f),
	FLinearColor(0.200000f, 0.000000f, 0.200000f),
	FLinearColor(0.200000f, 0.000000f, 0.000000f),
	FLinearColor(0.000000f, 1.000000f, 1.000000f),
	FLinearColor(0.000000f, 1.000000f, 0.800000f),
	FLinearColor(0.000000f, 1.000000f, 0.600000f),
	FLinearColor(0.000000f, 1.000000f, 0.400000f),
	FLinearColor(0.000000f, 1.000000f, 0.200000f),
	FLinearColor(0.000000f, 1.000000f, 0.000000f),
	FLinearColor(0.000000f, 0.800000f, 1.000000f),
	FLinearColor(0.000000f, 0.800000f, 0.800000f),
	FLinearColor(0.000000f, 0.800000f, 0.600000f),
	FLinearColor(0.000000f, 0.800000f, 0.400000f),
	FLinearColor(0.000000f, 0.800000f, 0.200000f),
	FLinearColor(0.000000f, 0.800000f, 0.000000f),
	FLinearColor(0.000000f, 0.600000f, 1.000000f),
	FLinearColor(0.000000f, 0.600000f, 0.800000f),
	FLinearColor(0.000000f, 0.600000f, 0.600000f),
	FLinearColor(0.000000f, 0.600000f, 0.400000f),
	FLinearColor(0.000000f, 0.600000f, 0.200000f),
	FLinearColor(0.000000f, 0.600000f, 0.000000f),
	FLinearColor(0.000000f, 0.400000f, 1.000000f),
	FLinearColor(0.000000f, 0.400000f, 0.800000f),
	FLinearColor(0.000000f, 0.400000f, 0.600000f),
	FLinearColor(0.000000f, 0.400000f, 0.400000f),
	FLinearColor(0.000000f, 0.400000f, 0.200000f),
	FLinearColor(0.000000f, 0.400000f, 0.000000f),
	FLinearColor(0.000000f, 0.200000f, 1.000000f),
	FLinearColor(0.000000f, 0.200000f, 0.800000f),
	FLinearColor(0.000000f, 0.200000f, 0.600000f),
	FLinearColor(0.000000f, 0.200000f, 0.400000f),
	FLinearColor(0.000000f, 0.200000f, 0.200000f),
	FLinearColor(0.000000f, 0.200000f, 0.000000f),
	FLinearColor(0.000000f, 0.000000f, 1.000000f),
	FLinearColor(0.000000f, 0.000000f, 0.800000f),
	FLinearColor(0.000000f, 0.000000f, 0.600000f),
	FLinearColor(0.000000f, 0.000000f, 0.400000f),
	FLinearColor(0.000000f, 0.000000f, 0.200000f),
	FLinearColor(0.933333f, 0.000000f, 0.000000f),
	FLinearColor(0.866667f, 0.000000f, 0.000000f),
	FLinearColor(0.733333f, 0.000000f, 0.000000f),
	FLinearColor(0.666667f, 0.000000f, 0.000000f),
	FLinearColor(0.533333f, 0.000000f, 0.000000f),
	FLinearColor(0.466667f, 0.000000f, 0.000000f),
	FLinearColor(0.333333f, 0.000000f, 0.000000f),
	FLinearColor(0.266667f, 0.000000f, 0.000000f),
	FLinearColor(0.133333f, 0.000000f, 0.000000f),
	FLinearColor(0.066667f, 0.000000f, 0.000000f),
	FLinearColor(0.000000f, 0.933333f, 0.000000f),
	FLinearColor(0.000000f, 0.866667f, 0.000000f),
	FLinearColor(0.000000f, 0.733333f, 0.000000f),
	FLinearColor(0.000000f, 0.666667f, 0.000000f),
	FLinearColor(0.000000f, 0.533333f, 0.000000f),
	FLinearColor(0.000000f, 0.466667f, 0.000000f),
	FLinearColor(0.000000f, 0.333333f, 0.000000f),
	FLinearColor(0.000000f, 0.266667f, 0.000000f),
	FLinearColor(0.000000f, 0.133333f, 0.000000f),
	FLinearColor(0.000000f, 0.066667f, 0.000000f),
	FLinearColor(0.000000f, 0.000000f, 0.933333f),
	FLinearColor(0.000000f, 0.000000f, 0.866667f),
	FLinearColor(0.000000f, 0.000000f, 0.733333f),
	FLinearColor(0.000000f, 0.000000f, 0.666667f),
	FLinearColor(0.000000f, 0.000000f, 0.533333f),
	FLinearColor(0.000000f, 0.000000f, 0.466667f),
	FLinearColor(0.000000f, 0.000000f, 0.333333f),
	FLinearColor(0.000000f, 0.000000f, 0.266667f),
	FLinearColor(0.000000f, 0.000000f, 0.133333f),
	FLinearColor(0.000000f, 0.000000f, 0.066667f),
	FLinearColor(0.933333f, 0.933333f, 0.933333f),
	FLinearColor(0.866667f, 0.866667f, 0.866667f),
	FLinearColor(0.733333f, 0.733333f, 0.733333f),
	FLinearColor(0.666667f, 0.666667f, 0.666667f),
	FLinearColor(0.533333f, 0.533333f, 0.533333f),
	FLinearColor(0.466667f, 0.466667f, 0.466667f),
	FLinearColor(0.333333f, 0.333333f, 0.333333f),
	FLinearColor(0.266667f, 0.266667f, 0.266667f),
	FLinearColor(0.133333f, 0.133333f, 0.133333f),
	FLinearColor(0.066667f, 0.066667f, 0.066667f),
	FLinearColor(0.000000f, 0.000000f, 0.000000f)
};

MagicaVoxelChunk::MagicaVoxelChunk(int NewModelID)
{
	ModelID = NewModelID;
	Materials.Init(FLinearColor::Black, 256);
}

void BinaryUtils::IncrementPointer(const uint8*& Src, const int NumBytes)
{
	Src += NumBytes;
}

void BinaryUtils::ReadInt32(int32* Dest, const uint8*& Src)
{
	if (Dest != nullptr)
	{
		memcpy(Dest, Src, sizeof(int32));
	}

	IncrementPointer(Src, 4);
}

void BinaryUtils::ReadSingle(float* Dest, const uint8*& Src)
{
	if (Dest != nullptr)
	{
		memcpy(Dest, Src, sizeof(float));
	}

	IncrementPointer(Src, 4);
}

void BinaryUtils::ReadBytes(uint8* Dest, const uint8*& Src, const int NumBytes)
{
	if (Dest != nullptr)
	{
		memcpy(Dest, Src, NumBytes * sizeof(uint8));
	}

	IncrementPointer(Src, NumBytes);
}

bool BinaryUtils::CompareData(const uint8* SourceData, const char* DataToCompare, const int NumBytes)
{
	for (int i = 0; i < NumBytes; ++i)
	{
		if (SourceData[i] != DataToCompare[i])
		{
			return false;
		}
	}

	return true;
}

EMagicaVoxelErrorCodes MagicaVoxelImporter::LoadVOX(
	const FString& RelativePath,
	bool bCreateInstances,
	TArray<MagicaVoxelInstance*>& OutInstances,
	TArray<VoxelModel>& OutVoxelModels
)
{
	bool FileExists = false;
	FString FullPath = RelativePath;// AssetID(RelativePath)

	if (!FullPath.IsEmpty())
	{
		FileExists = FPaths::FileExists(FullPath);
	}

	if (FileExists)
	{
		TArray64<uint8> Bytes;
		bool FileLoaded = FFileHelper::LoadFileToArray(Bytes, *FullPath);

		if (!BinaryUtils::CompareData(Bytes.GetData(), "VOX ", 4))
		{
			UE_LOG(LogTemp, Error, TEXT("Invalid VOX file, magic number mismatch"));
			return EMagicaVoxelErrorCodes::NotAMagicaFile;
		}

		return LoadVOXFromData(Bytes, bCreateInstances, RelativePath, OutInstances, OutVoxelModels);
	}
	else
	{
		UE_LOG(LogTemp, Error, TEXT("VOX file does not exists: %s"), *FullPath);
		return EMagicaVoxelErrorCodes::FileDoesNotExist;
	}
}

EMagicaVoxelErrorCodes MagicaVoxelImporter::LoadVOXFromData(
	const TArray64<uint8>& Data,
	const bool bCreateInstances,
	const FString& RelativePath,
	TArray<MagicaVoxelInstance*>& OutInstances,
	TArray<VoxelModel>& OutVoxelModels
)
{
	TArray<MagicaVoxelChunk*> VoxelModels;
	TArray<MagicaVoxelHierarchyNode*> VOXHierarchyFlat;
	TMap<int, EMagicaVoxelLayerType> Layers;

	const uint8* DataPtr = Data.GetData();

	// "VOX "
	BinaryUtils::IncrementPointer(DataPtr, 4);

	int32 Version;
	BinaryUtils::ReadInt32(&Version, DataPtr);
	UE_LOG(LogTemp, Log, TEXT("File version: %d."), Version);

	if (Version > MaxSupportedVersion)
	{
		UE_LOG(LogTemp, Error, TEXT("VOX files above version: %d are not supported!"), MaxSupportedVersion);
		return EMagicaVoxelErrorCodes::WrongVersion;
	}

	uint8 ChunkID[4];
	BinaryUtils::ReadBytes(ChunkID, DataPtr, 4);

	if (!BinaryUtils::CompareData(ChunkID, "MAIN", 4))
	{
		UE_LOG(LogTemp, Error, TEXT("[MVImport] Invalid MainChunk ID, main chunk expected"));
		return EMagicaVoxelErrorCodes::InvalidMainChunkID;
	}

	int32 ChunkSize;
	BinaryUtils::ReadInt32(&ChunkSize, DataPtr);

	int32 ChildrenSize;
	BinaryUtils::ReadInt32(&ChildrenSize, DataPtr);

	// main chunk should have nothing... skip
	BinaryUtils::IncrementPointer(DataPtr, ChunkSize);

	MagicaVoxelChunk* CurrentChunk = nullptr;
	int32 ModelID = 0;
	FLinearColor Materials[256];

	int32 ReadSize = 0;
	while (ReadSize < ChildrenSize)
	{
		BinaryUtils::ReadBytes(ChunkID, DataPtr, 4);

		if (BinaryUtils::CompareData(ChunkID, "PACK", 4))
		{
			int32 ChunkContentBytes;
			BinaryUtils::ReadInt32(&ChunkContentBytes, DataPtr);

			int32 ChildrenBytes;
			BinaryUtils::ReadInt32(&ChildrenBytes, DataPtr);

			BinaryUtils::IncrementPointer(DataPtr, 4);

			ReadSize += ChunkContentBytes + ChildrenBytes + 4 * 3;
		}
		else if (BinaryUtils::CompareData(ChunkID, "SIZE", 4))
		{
			if (CurrentChunk == nullptr)
			{
				CurrentChunk = new MagicaVoxelChunk(ModelID);
			}

			ReadSize += ReadSizeChunk(DataPtr, CurrentChunk);
		}
		else if (BinaryUtils::CompareData(ChunkID, "XYZI", 4))
		{
			if (CurrentChunk == nullptr)
			{
				UE_LOG(LogTemp, Log, TEXT("[MVImport] couldn't find SIZE block for modelID: %d"), VoxelModels.Num());
				CurrentChunk = new MagicaVoxelChunk(ModelID);
			}

			ReadSize += ReadVoxelChunk(DataPtr, CurrentChunk);

			VoxelModels.Add(CurrentChunk);

			ModelID++;
			CurrentChunk = nullptr;
		}
		else if (BinaryUtils::CompareData(ChunkID, "RGBA", 4))
		{
			FLinearColor Palette[256];
			ReadSize += ReadPalette(DataPtr, Palette);

			//for all models in the list
			for (MagicaVoxelChunk* const Model : VoxelModels)
			{
				Model->Palette.Insert(Palette, 256, 0);
			}
		}
		else if (BinaryUtils::CompareData(ChunkID, "MATT", 4))
		{
			ReadSize += ReadMaterialMATT(DataPtr, Materials);

			//for all models in the list
			for (MagicaVoxelChunk* const Model : VoxelModels)
			{
				Model->Materials.Insert(Materials, 256, 0);
			}
		}
		else if (BinaryUtils::CompareData(ChunkID, "MATL", 4))
		{
			ReadSize += ReadMaterialMATL(DataPtr, Materials);

			//for all models in the list
			for (MagicaVoxelChunk* const Model : VoxelModels)
			{
				Model->Materials.Insert(Materials, 256, 0);
			}
		}
		else if (BinaryUtils::CompareData(ChunkID, "LAYR", 4))
		{
			ReadSize += ReadLAYR(DataPtr, Layers);
		}
		else if (BinaryUtils::CompareData(ChunkID, "nTRN", 4))
		{
			ReadSize += ReadHierarchyTransform(DataPtr, VOXHierarchyFlat);
		}
		else if (BinaryUtils::CompareData(ChunkID, "nGRP", 4))
		{
			ReadSize += ReadHierarchyGroup(DataPtr, VOXHierarchyFlat);
		}
		else if (BinaryUtils::CompareData(ChunkID, "nSHP", 4))
		{
			ReadSize += ReadHierarchyShape(DataPtr, VOXHierarchyFlat);
		}
		else
		{
			UE_LOG(LogTemp, Log, TEXT("[MVImport] Chunk ID not recognized, got %c"), ChunkID);

			int32 ChunkContentBytes;
			BinaryUtils::ReadInt32(&ChunkContentBytes, DataPtr);

			int32 ChildrenBytes;
			BinaryUtils::ReadInt32(&ChildrenBytes, DataPtr);

			BinaryUtils::IncrementPointer(DataPtr, ChunkContentBytes + ChildrenBytes);
			ReadSize += ChunkContentBytes + ChildrenBytes + 12;
		}
	}

	for (MagicaVoxelChunk* const Model : VoxelModels)
	{
		if (Model->Palette.Num() == 0)
			Model->Palette = MagicaVoxelChunk::DefaultPalette;
	}

	if (VOXHierarchyFlat.Num() == 0)//old vox format with single model
	{
		MagicaVoxelHierarchyNode* NewNode = new MagicaVoxelHierarchyNode();
		NewNode->NodeID = 0;
		NewNode->ModelID = 0;
		NewNode->Layer = -1;
		VOXHierarchyFlat.Add(NewNode);
	}

	MagicaVoxelHierarchyNode RootNode = ResolveHierarchy(VOXHierarchyFlat, VoxelModels);
	CreateInstances(RootNode, FMatrix::Identity, FMatrix::Identity, VoxelModels, OutInstances);

	if (bCreateInstances)
	{
		CreateVoxelModels(OutInstances, RelativePath, Layers, OutVoxelModels);
	}

	return EMagicaVoxelErrorCodes::NoError;
}

int MagicaVoxelImporter::ReadSizeChunk(const uint8*& DataPtr, MagicaVoxelChunk* const Chunk)
{
	int32 ChunkSize;
	BinaryUtils::ReadInt32(&ChunkSize, DataPtr);

	int32 ChildrenSize;
	BinaryUtils::ReadInt32(&ChildrenSize, DataPtr);

	BinaryUtils::ReadInt32(&Chunk->SizeX, DataPtr);
	BinaryUtils::ReadInt32(&Chunk->SizeY, DataPtr);
	BinaryUtils::ReadInt32(&Chunk->SizeZ, DataPtr);

	if (ChildrenSize > 0)
	{
		BinaryUtils::IncrementPointer(DataPtr, ChildrenSize);
		UE_LOG(LogTemp, Warning, TEXT("Nested chunk not supported"));
	}

	return ChunkSize + ChildrenSize + 4 * 3; //4 * 3 is chunkId, chunkSize, chunkChildren
}

int MagicaVoxelImporter::ReadVoxelChunk(const uint8*& DataPtr, MagicaVoxelChunk* const Chunk)
{
	int32 ChunkSize;
	BinaryUtils::ReadInt32(&ChunkSize, DataPtr);

	int32 ChildrenSize;
	BinaryUtils::ReadInt32(&ChildrenSize, DataPtr);

	int32 NumVoxels;
	BinaryUtils::ReadInt32(&NumVoxels, DataPtr);
	
	Chunk->NumVoxels = NumVoxels;
	Chunk->Voxels.AddDefaulted(NumVoxels);

	for (int32 i = 0; i < NumVoxels; ++i)
	{
		Chunk->Voxels[i] = new MagicaVoxelVoxel();

		BinaryUtils::ReadBytes(&Chunk->Voxels[i]->X, DataPtr, 1);
		BinaryUtils::ReadBytes(&Chunk->Voxels[i]->Y, DataPtr, 1);
		BinaryUtils::ReadBytes(&Chunk->Voxels[i]->Z, DataPtr, 1);
		BinaryUtils::ReadBytes(&Chunk->Voxels[i]->ColorIndex, DataPtr, 1);
	}

	if (ChildrenSize > 0)
	{
		BinaryUtils::IncrementPointer(DataPtr, ChildrenSize);
		UE_LOG(LogTemp, Warning, TEXT("Nested chunk not supported"));
	}

	return ChunkSize + ChildrenSize + 4 * 3; //4 * 3 is chunkId, chunkSize, chunkChildren
}

int MagicaVoxelImporter::ReadString(const uint8*& DataPtr, FString& OutString)
{
	int32 BufferSize;
	BinaryUtils::ReadInt32(&BufferSize, DataPtr);

	uint8* Bytes = new uint8[BufferSize+1];
	BinaryUtils::ReadBytes(Bytes, DataPtr, BufferSize);
	Bytes[BufferSize] = '\0';

	OutString = FString(UTF8_TO_TCHAR(Bytes));

	delete[] Bytes;

	return BufferSize + 4;
}

int MagicaVoxelImporter::ReadDictionary(const uint8*& DataPtr, TMap<FString, FString>& OutDictionary)
{
	int32 DictionaryEntries;
	BinaryUtils::ReadInt32(&DictionaryEntries, DataPtr);

	int32 ReadBytes = 4;

	for (int32 i = 0; i < DictionaryEntries; ++i)
	{
		FString Key;
		FString Value;

		ReadBytes += ReadString(DataPtr, Key);
		ReadBytes += ReadString(DataPtr, Value);

		OutDictionary.Add(Key, Value);
	}

	return ReadBytes;
}

int MagicaVoxelImporter::ReadPalette(const uint8*& DataPtr, FLinearColor* const OutColors)
{
	int32 ChunkSize;
	BinaryUtils::ReadInt32(&ChunkSize, DataPtr);

	int32 ChildrenSize;
	BinaryUtils::ReadInt32(&ChildrenSize, DataPtr);

	OutColors[0] = FLinearColor::Black;

	for (int32 i = 0; i <= 254; ++i)
	{
		uint8 r, g, b, a;
		BinaryUtils::ReadBytes(&r, DataPtr, 1);
		BinaryUtils::ReadBytes(&g, DataPtr, 1);
		BinaryUtils::ReadBytes(&b, DataPtr, 1);
		BinaryUtils::ReadBytes(&a, DataPtr, 1);

		OutColors[i + 1] = FLinearColor(r / 255.0, g / 255.0, b / 255.0, a / 255.0);
	}

	BinaryUtils::IncrementPointer(DataPtr, 4);

	if (ChildrenSize > 0)
	{
		BinaryUtils::IncrementPointer(DataPtr, ChildrenSize);
		UE_LOG(LogTemp, Warning, TEXT("Nested chunk not supported"));
	}

	return ChunkSize + ChildrenSize + 4 * 3; //4 * 3 is chunkId, chunkSize, chunkChildren
}

int MagicaVoxelImporter::ReadMaterialMATT(const uint8*& DataPtr, FLinearColor* const OutMaterials)
{
	int32 ChunkSize;
	BinaryUtils::ReadInt32(&ChunkSize, DataPtr);

	int32 ChildrenSize;
	BinaryUtils::ReadInt32(&ChildrenSize, DataPtr);

	FLinearColor Material = FLinearColor::Black;

	//4 | int | id[1 - 255]
	int32 MaterialIndex;
	BinaryUtils::ReadInt32(&MaterialIndex, DataPtr);

	/*4 | int | material type
			  | 0 : diffuse
			  | 1 : metal
			  | 2 : glass
			  | 3 : emissive*/
	int32 MaterialType;
	BinaryUtils::ReadInt32(&MaterialType, DataPtr);

	/*        4 | float | material weight
						| diffuse  : 1.0
						| metal    : (0.0 - 1.0] : blend between metal and diffuse material
						| glass    : (0.0 - 1.0] : blend between glass and diffuse material
						| emissive : (0.0 - 1.0] : self - illuminated material*/

	float MaterialWeight;
	BinaryUtils::ReadSingle(&MaterialWeight, DataPtr);

	if (MaterialType == 1)
		Material.R = MaterialWeight; //metalness
	else if (MaterialType == 2)
		Material.A = MaterialWeight; //transparency
	else if (MaterialType == 3)
		Material.B = MaterialWeight; //emissive


	/*        4 | int | property bits: set if value is saved in next section
								  | bit(0) : Plastic
								  | bit(1) : Roughness
								  | bit(2) : Specular
								  | bit(3) : IOR
								  | bit(4) : Attenuation
								  | bit(5) : Power
								  | bit(6) : Glow
								  | bit(7) : isTotalPower(*no value)
	*/
	int32 PropertyBits;
	BinaryUtils::ReadInt32(&PropertyBits, DataPtr);

	/*        4 * N | float | normalized property value : (0.0 - 1.0]
							| *need to map to real range
							| *Plastic material only accepts { 0.0, 1.0}*/

	for (int i = 0; i < 8; i++)
	{
		if (((PropertyBits >> i) & 0x1) == 1)
		{
			float NormalizedPropertyValue;
			BinaryUtils::ReadSingle(&NormalizedPropertyValue, DataPtr);

			if (i == 1) //roughness
			{
				Material.G = 1.0f - NormalizedPropertyValue; //metalness
			}
		}
	}

	OutMaterials[MaterialIndex] = Material;

	if (ChildrenSize > 0)
	{
		BinaryUtils::IncrementPointer(DataPtr, ChildrenSize);
		UE_LOG(LogTemp, Warning, TEXT("Nested chunk not supported"));
	}

	return ChunkSize + ChildrenSize + 4 * 3; //4 * 3 is chunkId, chunkSize, chunkChildren
}

int MagicaVoxelImporter::ReadMaterialMATL(const uint8*& DataPtr, FLinearColor* const OutMaterials)
{
	int32 ChunkSize;
	BinaryUtils::ReadInt32(&ChunkSize, DataPtr);

	int32 ChildrenSize;
	BinaryUtils::ReadInt32(&ChildrenSize, DataPtr);

	FLinearColor Material = FLinearColor::Black;

	int32 MaterialIndex;
	BinaryUtils::ReadInt32(&MaterialIndex, DataPtr);

	int32 MaterialType = -1;

	TMap<FString, FString> MaterialProperties;
	ReadDictionary(DataPtr, MaterialProperties);

	for (const TPair<FString, FString>& Element : MaterialProperties)
	{
		if (Element.Key.Compare("_type") == 0)
		{
			if (Element.Value.Compare("_diffuse") == 0)
			{
				MaterialType = 0;
			}
			else if (Element.Value.Compare("_metal") == 0)
			{
				MaterialType = 1;
			}
			else if (Element.Value.Compare("_glass") == 0)
			{
				MaterialType = 2;
			}
			else if (Element.Value.Compare("_emit") == 0)
			{
				MaterialType = 3;
			}
		}
		else if (Element.Key.Compare("_weight") == 0)
		{
			float MaterialWeight = FCString::Atof(*Element.Value);

			if (MaterialType == 1)
			{
				Material.R = MaterialWeight; //metalness
			}
			else if (MaterialType == 2)
			{
				Material.A = MaterialWeight; //transparency
			}
			else if (MaterialType == 3)
			{
				Material.B = MaterialWeight; //emissive
			}
		}
		else if (Element.Key.Compare("_rough") == 0)
		{
			float MaterialRoughness = FCString::Atof(*Element.Value);
			Material.G = MaterialRoughness; //roughness
		}
		//skip rest
	}

	if (MaterialIndex > 255)
	{
		UE_LOG(LogTemp, Warning, TEXT("MaterialIndex = %d"), MaterialIndex);
	}
	else
	{
		OutMaterials[MaterialIndex] = Material;
	}

	if (ChildrenSize > 0)
	{
		BinaryUtils::IncrementPointer(DataPtr, ChildrenSize);
		UE_LOG(LogTemp, Warning, TEXT("Nested chunk not supported"));
	}

	return ChunkSize + ChildrenSize + 4 * 3; //4 * 3 is chunkId, chunkSize, chunkChildren
}

int MagicaVoxelImporter::ReadLAYR(const uint8*& DataPtr, TMap<int, EMagicaVoxelLayerType>& OutLayers)
{
	int32 ChunkSize;
	BinaryUtils::ReadInt32(&ChunkSize, DataPtr);

	int32 ChildrenSize;
	BinaryUtils::ReadInt32(&ChildrenSize, DataPtr);

	int32 LayerID;
	BinaryUtils::ReadInt32(&LayerID, DataPtr);

	TMap<FString, FString> LayerAttributes;
	ReadDictionary(DataPtr, LayerAttributes);

	EMagicaVoxelLayerType Layer = EMagicaVoxelLayerType::LayerNone; //frozen

	for (const TPair<FString, FString>& Element : LayerAttributes)
	{
		if (Element.Key.Compare("_name") == 0)
		{
			if (Element.Value.Compare("static", ESearchCase::IgnoreCase) == 0)
			{
				Layer = EMagicaVoxelLayerType::LayerStatic;
			}
			else if (Element.Key.Compare("dynamic", ESearchCase::IgnoreCase) == 0)
			{
				Layer = EMagicaVoxelLayerType::LayerDynamic;
			}
			else if (Element.Key.Compare("water", ESearchCase::IgnoreCase) == 0)
			{
				Layer = EMagicaVoxelLayerType::LayerWater;
			}
			else if (Element.Key.Compare("hardsurface", ESearchCase::IgnoreCase) == 0)
			{
				Layer = EMagicaVoxelLayerType::LayerHardSurface;
			}
		}
	}

	OutLayers.Add(LayerID, Layer);

	//read reserved
	BinaryUtils::IncrementPointer(DataPtr, 4);

	return ChunkSize + ChildrenSize + 4 * 3; //4 * 3 is chunkId, chunkSize, chunkChildren
}

int MagicaVoxelImporter::ReadHierarchyTransform(const uint8*& DataPtr, TArray<MagicaVoxelHierarchyNode*>& OutHierarchy)
{
	int32 ChunkContentBytes;
	BinaryUtils::ReadInt32(&ChunkContentBytes, DataPtr);

	int32 ChildrenBytes;
	BinaryUtils::ReadInt32(&ChildrenBytes, DataPtr);

	MagicaVoxelHierarchyNode* NewNode = new MagicaVoxelHierarchyNode();
	BinaryUtils::ReadInt32(&NewNode->NodeID, DataPtr);

	TMap<FString, FString> NodeAttributes;
	ReadDictionary(DataPtr, NodeAttributes);

	int32 ChildNodeID;
	BinaryUtils::ReadInt32(&ChildNodeID, DataPtr);

	NewNode->ChildrenIDs.Add(ChildNodeID);

	//reserved 4 bytes
	BinaryUtils::IncrementPointer(DataPtr, 4);

	BinaryUtils::ReadInt32(&NewNode->Layer, DataPtr);

	int32 NumFrames; //must be 1
	BinaryUtils::ReadInt32(&NumFrames, DataPtr);

	for (int32 i = 0; i < NumFrames; ++i)
	{
		TMap<FString, FString> FrameAttributes;
		ReadDictionary(DataPtr, FrameAttributes);

		FVector Translate = FVector::ZeroVector;
		FMatrix RotationMatrix = FMatrix::Identity;

		for (const TPair<FString, FString>& Element : FrameAttributes)
		{
			if (Element.Key.Compare("_r") == 0)
			{
				uint8 Rot = FCString::Atoi(*Element.Value);

				int32 Index0 = Rot & 0x3;
				int32 Index1 = (Rot >> 2) & 0x3;
				int32 Index2 = 0;

				//this matrix is orthogonal - find the third index pos
				if ((Index0 == 1 || Index0 == 2) && (Index1 == 1 || Index1 == 2))
				{
					Index2 = 0;
				}
				if ((Index0 == 0 || Index0 == 2) && (Index1 == 0 || Index1 == 2))
				{
					Index2 = 1;
				}
				if ((Index0 == 0 || Index0 == 1) && (Index1 == 0 || Index1 == 1))
				{
					Index2 = 2;
				}

				float Sign0 = ((Rot >> 4) & 0x1) == 1 ? -1.0f : 1.0f;
				float Sign1 = ((Rot >> 5) & 0x1) == 1 ? -1.0f : 1.0f;
				float Sign2 = ((Rot >> 6) & 0x1) == 1 ? -1.0f : 1.0f;

				FVector Row0 = FVector::ZeroVector;
				if (Index0 == 0)
				{
					Row0.X = 1.0f;
				}
				else if (Index0 == 1)
				{
					Row0.Y = 1.0f;
				}
				else if (Index0 == 2)
				{
					Row0.Z = 1.0f;
				}

				FVector Row1 = FVector::ZeroVector;
				if (Index1 == 0)
				{
					Row1.X = 1.0f;
				}
				else if (Index1 == 1)
				{
					Row1.Y = 1.0f;
				}
				else if (Index1 == 2)
				{
					Row1.Z = 1.0f;
				}

				FVector Row2 = FVector::ZeroVector;
				if (Index2 == 0)
				{
					Row2.X = 1.0f;
				}
				else if (Index2 == 1)
				{
					Row2.Y = 1.0f;
				}
				else if (Index2 == 2)
				{
					Row2.Z = 1.0f;
				}

				RotationMatrix.SetColumn(0, Row0 * Sign0);
				RotationMatrix.SetColumn(1, Row1 * Sign1);
				RotationMatrix.SetColumn(2, Row2 * Sign2);
			}
			else if (Element.Key.Compare("_t") == 0)
			{
				//split numbers
				TArray<FString> Numbers;
				Element.Value.ParseIntoArray(Numbers, TEXT(" "), true);

				int32 X = FCString::Atoi(*Numbers[0]);
				int32 Y = FCString::Atoi(*Numbers[1]);
				int32 Z = FCString::Atoi(*Numbers[2]);

				Translate = FVector(X, Y, Z);
			}
		}

		NewNode->TransformMatrix = FTranslationMatrix(Translate) * RotationMatrix;
		NewNode->RotationMatrix = RotationMatrix;
	}

	OutHierarchy.Add(NewNode);

	return ChunkContentBytes + ChildrenBytes + 12;
}

int MagicaVoxelImporter::ReadHierarchyGroup(const uint8*& DataPtr, TArray<MagicaVoxelHierarchyNode*>& OutHierarchy)
{
	int32 ChunkContentBytes;
	BinaryUtils::ReadInt32(&ChunkContentBytes, DataPtr);

	int32 ChildrenBytes;
	BinaryUtils::ReadInt32(&ChildrenBytes, DataPtr);

	MagicaVoxelHierarchyNode* NewNode = new MagicaVoxelHierarchyNode();

	int32 NodeID;
	BinaryUtils::ReadInt32(&NodeID, DataPtr);
	NewNode->NodeID = NodeID;

	TMap<FString, FString> NodeAttributes;
	ReadDictionary(DataPtr, NodeAttributes);

	int32 NumberOfChildren;
	BinaryUtils::ReadInt32(&NumberOfChildren, DataPtr);

	for (int32 i = 0; i < NumberOfChildren; ++i)
	{
		int32 ChildNodeID;
		BinaryUtils::ReadInt32(&ChildNodeID, DataPtr);
		NewNode->ChildrenIDs.Add(ChildNodeID);
	}

	OutHierarchy.Add(NewNode);

	return ChunkContentBytes + ChildrenBytes + 12;
}

int MagicaVoxelImporter::ReadHierarchyShape(const uint8*& DataPtr, TArray<MagicaVoxelHierarchyNode*>& OutHierarchy)
{
	int32 ChunkContentBytes;
	BinaryUtils::ReadInt32(&ChunkContentBytes, DataPtr);

	int32 ChildrenBytes;
	BinaryUtils::ReadInt32(&ChildrenBytes, DataPtr);

	MagicaVoxelHierarchyNode* NewNode = new MagicaVoxelHierarchyNode();

	BinaryUtils::ReadInt32(&NewNode->NodeID, DataPtr);
	TMap<FString, FString> NodeAttributes;
	ReadDictionary(DataPtr, NodeAttributes);

	int32 NumModels;
	BinaryUtils::ReadInt32(&NumModels, DataPtr);

	//must be 1
	for (int32 i = 0; i < NumModels; ++i)
	{
		int32 ModelID;
		BinaryUtils::ReadInt32(&ModelID, DataPtr);
		NewNode->ModelID = ModelID;

		TMap<FString, FString> ModelAttributes;
		ReadDictionary(DataPtr, ModelAttributes);
	}

	OutHierarchy.Add(NewNode);

	return ChunkContentBytes + ChildrenBytes + 12;
}

MagicaVoxelHierarchyNode MagicaVoxelImporter::ResolveHierarchy(
	TArray<MagicaVoxelHierarchyNode*>& VOXHierarchyFlat,
	const TArray<MagicaVoxelChunk*>& Models)
{
	MagicaVoxelHierarchyNode* RootNode = VOXHierarchyFlat[0];

	for (int32 i = 0; i < VOXHierarchyFlat.Num(); ++i)
	{
		if (VOXHierarchyFlat[i]->ChildrenIDs.Num() == 0)
		{
			if (VOXHierarchyFlat[i]->ModelID >= 0)
			{
				const MagicaVoxelChunk* Model = nullptr;

				for (int32 ModelNo = 0; ModelNo < Models.Num(); ++ModelNo)
				{
					if (Models[ModelNo]->ModelID == VOXHierarchyFlat[i]->ModelID)
					{
						Model = Models[ModelNo];
					}
				}
				if (Model != nullptr)
				{
					VOXHierarchyFlat[i]->TransformMatrix = FMatrix::Identity;
					VOXHierarchyFlat[i]->RotationMatrix = FMatrix::Identity;
				}
				else
				{
					VOXHierarchyFlat[i]->ModelID = -1;
				}
			}

			continue;
		}

		for (int32 Child = 0; Child < VOXHierarchyFlat[i]->ChildrenIDs.Num(); ++Child)
		{
			int32 ChildID = VOXHierarchyFlat[i]->ChildrenIDs[Child];

			for (int32 j = 0; j < VOXHierarchyFlat.Num(); ++j)
			{
				if (VOXHierarchyFlat[j]->NodeID == ChildID)
				{
					if (VOXHierarchyFlat[j]->Layer == -1)
					{
						VOXHierarchyFlat[j]->Layer = VOXHierarchyFlat[i]->Layer;
					}

					VOXHierarchyFlat[i]->Children.Add(VOXHierarchyFlat[j]);
					break;
				}
			}
		}
	}

	return *RootNode;
}

void MagicaVoxelImporter::CreateInstances(
	const MagicaVoxelHierarchyNode& Node,
	FMatrix Matrix,
	FMatrix RotationMatrix,
	const TArray<MagicaVoxelChunk*>& VoxelModels,
	TArray<MagicaVoxelInstance*>& OutInstances)
{
	if (Node.ModelID != -1 && VoxelModels[Node.ModelID]->NumVoxels > 0)
	{
		MagicaVoxelInstance* NewInstance = new MagicaVoxelInstance();
		NewInstance->FinalRotationMatrix = RotationMatrix;
		NewInstance->FinalMatrix = Matrix;
		NewInstance->Layer = Node.Layer;
		NewInstance->VoxelChunk = VoxelModels[Node.ModelID];

		OutInstances.Add(NewInstance);
	}
	else if (Node.Children.Num() > 0)
	{
		for (const MagicaVoxelHierarchyNode* Child : Node.Children)
		{
			FMatrix NewMatrix = Matrix * Node.TransformMatrix;
			FMatrix NewRotationMatrix = RotationMatrix * Node.RotationMatrix;
			CreateInstances(*Child, NewMatrix, NewRotationMatrix, VoxelModels, OutInstances);
		}
	}
}

void MagicaVoxelImporter::CreateVoxelModels(
	const TArray<MagicaVoxelInstance*> Instances,
	const FString RelativePath,
	const TMap<int, EMagicaVoxelLayerType> Layers,
	TArray<VoxelModel>& OutVoxelModels)
{
	for (int32 i = 0; i < Instances.Num(); ++i)
	{
		const MagicaVoxelInstance* Instance = Instances[i];

		int32 InstanceNumber = -1;

		for (int32 k = 0; k < i; ++k)
		{
			if (Instance->VoxelChunk == Instances[k]->VoxelChunk &&
				Instance->FinalRotationMatrix.Equals(Instances[k]->FinalRotationMatrix))
			{
				InstanceNumber = k;
				break;
			}
		}

		if (InstanceNumber == -1)
		{
			InstanceNumber = i;
		}

		FMatrix FinalMatrix = Instance->FinalMatrix;
		FMatrix FinalRotationMatrix = Instance->FinalRotationMatrix;

		EMagicaVoxelLayerType LayerType = EMagicaVoxelLayerType::LayerNone;
		if (Layers.Contains(Instance->Layer))
		{
			LayerType = Layers[Instance->Layer];
		}

		MagicaVoxelChunk* const Model = Instance->VoxelChunk;

		FVector VoxelOffset = FinalRotationMatrix.TransformVector(FVector(Model->SizeX, Model->SizeY, Model->SizeZ));
		VoxelOffset = VoxelOffset.ComponentMin(FVector::ZeroVector);
		FVector Pivot = FinalRotationMatrix.TransformVector(FVector(FMath::Floor(Model->SizeX / 2.0f), FMath::Floor(Model->SizeY / 2.0f), FMath::Floor(Model->SizeZ / 2.0f))) - VoxelOffset;
		FVector ZeroTranslation = FinalMatrix.TransformPosition(FVector::ZeroVector);
		ZeroTranslation.X = FMath::RoundToInt(ZeroTranslation.X);
		ZeroTranslation.Y = FMath::RoundToInt(ZeroTranslation.Y);
		ZeroTranslation.Z = FMath::RoundToInt(ZeroTranslation.Z);

		const float VoxelScale = MagicaVoxelManager::WorldVoxelScale;
		FVector Translation = ZeroTranslation * VoxelScale - Pivot * VoxelScale;

		float Density = 5.0f;

		if (LayerType == EMagicaVoxelLayerType::LayerWater)
		{
			Density = 1.0f;
		}

		VoxelModel VOXModel = VoxelModel::CreateVOXModel("MagicaVoxelWorld_" + InstanceNumber, RelativePath, InstanceNumber, LayerType, Density, Translation, FQuat::Identity);
		OutVoxelModels.Add(VOXModel);
	}
}

