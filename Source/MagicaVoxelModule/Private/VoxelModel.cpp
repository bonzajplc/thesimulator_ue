// Fill out your copyright notice in the Description page of Project Settings.


#include "VoxelModel.h"

#include "SimulatedObject.h"

VoxelModel VoxelModel::CreateVOXModel(
	const FString& Name,
	const FString& Path,
	const int InstanceID,
	const EMagicaVoxelLayerType LayerType,
	const float Density,
	const FVector LocalPosition,
	const FQuat LocalRotation)
{
	VoxelModel voxModel;
	voxModel.FilePath = Path;
	voxModel.InstanceID = InstanceID;

	if (LayerType == EMagicaVoxelLayerType::LayerStatic)
	{
		voxModel.SimulatedObjectState = ESimulatedObjectState::Static;
	}
	else if (LayerType == EMagicaVoxelLayerType::LayerDynamic ||
			 LayerType == EMagicaVoxelLayerType::LayerWater)
	{
		voxModel.SimulatedObjectState = ESimulatedObjectState::Awake;
	}
	else
	{
		voxModel.SimulatedObjectState = ESimulatedObjectState::Frozen;
	}

	voxModel.Density = Density;
	voxModel.LayerType = LayerType;

	if (LayerType != EMagicaVoxelLayerType::LayerStatic &&
		LayerType != EMagicaVoxelLayerType::LayerWater)
	{
		voxModel.SubdivisionX = 32;
		voxModel.SubdivisionY = 32;
		voxModel.SubdivisionZ = 32;
	}
	else
	{
		voxModel.SubdivisionX = 256;
		voxModel.SubdivisionY = 256;
		voxModel.SubdivisionZ = 256;
	}

	if (LayerType == EMagicaVoxelLayerType::LayerWater)
	{
		voxModel.WakeOthers = false;
		voxModel.DestroyUnits = EDestroyUnitsType::None;
	}

	voxModel.TransformPosition = LocalPosition;
	voxModel.TransformRotation = LocalRotation;
	voxModel.TransformScale = FVector::OneVector;

	return voxModel;
}

VoxelModel::VoxelModel()
{
}

VoxelModel::~VoxelModel()
{
}
