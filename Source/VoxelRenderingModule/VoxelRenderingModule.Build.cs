using UnrealBuildTool;
using System.IO;

public class VoxelRenderingModule : ModuleRules
{
    public VoxelRenderingModule (ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "RenderCore",
            "HeadMountedDisplay",
            "NavigationSystem",
            "AIModule",
            "RHI",
            "Renderer",
            "MagicaVoxelModule"
        });

        string EnginePath = Path.GetFullPath(Target.RelativeEnginePath);
        string SrcRuntimePath = EnginePath + "Source/Runtime/";
        PublicIncludePaths.AddRange(new string[] {SrcRuntimePath + "Renderer/Private"});
        PrivateIncludePaths.AddRange(new string[] { SrcRuntimePath + "Renderer/Private"});
    }
}