// Fill out your copyright notice in the Description page of Project Settings.


#include "VoxelManager.h"
#include "EditorViewportClient.h"

#include "GlobalShader.h"
#include "ShaderParameterStruct.h"
#include "RenderGraphUtils.h"
#include "RenderResource.h"
#include "Modules/ModuleManager.h"
#include "DynamicRHI.h"
#include "RHICommandList.h"
#include <vector>
#include "PixelShaderUtils.h"
#include "PostProcess/SceneRenderTargets.h"
#include "ClearQuad.h"

#include "DrawDebugHelpers.h"	// For DrawDebugSphere

#define NUM_THREADS_PER_GROUP 256

#define NUM_CUBES_X 100
#define NUM_CUBES_Y 100
#define NUM_CUBES_Z 100
#define NUM_CUBES (NUM_CUBES_X * NUM_CUBES_Y * NUM_CUBES_Z)

#define NUM_MAX_VOXELS (4 * 1024 * 1024)

#define NUM_CULL_PLANES 6

struct FVoxelInfo
{
	uint32_t VoxelPosXYZ_Face; //9 bit X, Y, Z; 5 bit voxel face
	uint32_t VoxelSizeWH_MaterialData; //8 bit W, H; 8 bit metalness, 8 bit shininess
	uint32_t VoxelColorRGBA;
	uint32_t InstanceID;
};

class FVoxelCullCS : public FGlobalShader
{
private:

	DECLARE_GLOBAL_SHADER(FVoxelCullCS);
	SHADER_USE_PARAMETER_STRUCT(FVoxelCullCS, FGlobalShader);

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_SRV(StructuredBuffer<float3>, Positions)
		SHADER_PARAMETER_SRV(StructuredBuffer<uint>, InputVoxels)
		SHADER_PARAMETER_SRV(StructuredBuffer<uint>, InputVoxelsColors)
		SHADER_PARAMETER_UAV(RWStructuredBuffer<VoxelInfo>, OutputVoxels)
		SHADER_PARAMETER_UAV(RWBuffer<uint32_t>, OutputVoxelsCounter)
		SHADER_PARAMETER(uint32_t, NumVoxels)
		SHADER_PARAMETER(uint32_t, NumInstances)
		SHADER_PARAMETER(uint32_t, InstanceOffset)
		SHADER_PARAMETER(FMatrix, ViewProjectionMatrix)
		SHADER_PARAMETER(FVector, ViewOrigin)
		SHADER_PARAMETER(FVector, PreViewTranslation)
		SHADER_PARAMETER(float, VoxelScale)
		END_SHADER_PARAMETER_STRUCT()

public:

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static bool ShouldCache(EShaderPlatform Platform)
	{
		return true;
	}

	static inline void
		ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters,
			FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);

		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_X"), NUM_THREADS_PER_GROUP);
		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_Y"), 1);
		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_Z"), 1);
	}
};

IMPLEMENT_SHADER_TYPE(, FVoxelCullCS, TEXT("/CustomShaders/VoxelRendering/Voxel_Culling.usf"), TEXT("VoxelCullCS"), SF_Compute);

class FVoxelCullClearCS : public FGlobalShader
{
	DECLARE_GLOBAL_SHADER(FVoxelCullClearCS);
	SHADER_USE_PARAMETER_STRUCT(FVoxelCullClearCS, FGlobalShader);

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_UAV(RWBuffer<uint32_t>, OutputVoxelsCounter)
	END_SHADER_PARAMETER_STRUCT()

public:
	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static void ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters, FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);
	}
};

IMPLEMENT_SHADER_TYPE(, FVoxelCullClearCS, TEXT("/CustomShaders/VoxelRendering/Voxel_Culling.usf"), TEXT("VoxelCullClearCS"), SF_Compute);

class FVoxelBasePass : public FGlobalShader
{
public:

	SHADER_USE_PARAMETER_STRUCT(FVoxelBasePass, FGlobalShader);

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		RENDER_TARGET_BINDING_SLOTS()
//		SHADER_PARAMETER_UAV(RWStructuredBuffer<float2>, Dimensions)
		SHADER_PARAMETER_SRV(StructuredBuffer<float4>, Voxels)
		SHADER_PARAMETER_SRV(StructuredBuffer<float3>, Colors)
		SHADER_PARAMETER_SRV(StructuredBuffer<float3>, Positions)
		SHADER_PARAMETER_SRV(StructuredBuffer<float4>, Rotations)
		SHADER_PARAMETER(FMatrix, ViewProjectionMatrix)
		SHADER_PARAMETER(FVector, PreViewTranslation)
		SHADER_PARAMETER(float, VoxelScale)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(FGlobalShaderPermutationParameters const& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}
};

class FVoxelBasePassVS : public FVoxelBasePass
{
public:

	DECLARE_GLOBAL_SHADER(FVoxelBasePassVS);

	FVoxelBasePassVS() {}
	FVoxelBasePassVS(const ShaderMetaType::CompiledShaderInitializerType& Initializer) : FVoxelBasePass(Initializer) { }
};

IMPLEMENT_SHADER_TYPE(, FVoxelBasePassVS, TEXT("/CustomShaders/VoxelRendering/Voxel_BasePass.usf"), TEXT("MainVertexShader"), SF_Vertex);

class FVoxelBasePassGS : public FVoxelBasePass
{
public:

	DECLARE_GLOBAL_SHADER(FVoxelBasePassGS);

	FVoxelBasePassGS() {}
	FVoxelBasePassGS(const ShaderMetaType::CompiledShaderInitializerType& Initializer) : FVoxelBasePass(Initializer) { }
};

IMPLEMENT_SHADER_TYPE(, FVoxelBasePassGS, TEXT("/CustomShaders/VoxelRendering/Voxel_BasePass.usf"), TEXT("MainGeometryShader"), SF_Geometry);

class FVoxelBasePassPS : public FVoxelBasePass
{
public:

	DECLARE_GLOBAL_SHADER(FVoxelBasePassPS);

	FVoxelBasePassPS() {}
	FVoxelBasePassPS(const ShaderMetaType::CompiledShaderInitializerType& Initializer) : FVoxelBasePass(Initializer) { }
};

IMPLEMENT_SHADER_TYPE(, FVoxelBasePassPS, TEXT("/CustomShaders/VoxelRendering/Voxel_BasePass.usf"), TEXT("MainPixelShader"), SF_Pixel);

class FVoxelShadowPass : public FGlobalShader
{
public:

	SHADER_USE_PARAMETER_STRUCT(FVoxelShadowPass, FGlobalShader);

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		RENDER_TARGET_BINDING_SLOTS()
		SHADER_PARAMETER_SRV(StructuredBuffer<float4>, Voxels)
		SHADER_PARAMETER_SRV(StructuredBuffer<float3>, Positions)
		SHADER_PARAMETER_SRV(StructuredBuffer<float4>, Rotations)
		SHADER_PARAMETER(FMatrix, ViewProjectionMatrix)
		SHADER_PARAMETER(FMatrix, ShadowMatrix)
		SHADER_PARAMETER(FVector, PreViewTranslation)
		SHADER_PARAMETER(FVector4, ShadowParams)
		SHADER_PARAMETER(float, bClampToNearPlane)
		SHADER_PARAMETER(float, VoxelScale)
	END_SHADER_PARAMETER_STRUCT()

	static bool ShouldCompilePermutation(FGlobalShaderPermutationParameters const& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}
};

class FVoxelShadowPassVS : public FVoxelShadowPass
{
public:

	DECLARE_GLOBAL_SHADER(FVoxelShadowPassVS);

	FVoxelShadowPassVS() {}
	FVoxelShadowPassVS(const ShaderMetaType::CompiledShaderInitializerType& Initializer) : FVoxelShadowPass(Initializer) { }
};

IMPLEMENT_SHADER_TYPE(, FVoxelShadowPassVS, TEXT("/CustomShaders/VoxelRendering/Voxel_ShadowPass.usf"), TEXT("MainVertexShader"), SF_Vertex);

class FVoxelShadowPassGS : public FVoxelShadowPass
{
public:

	DECLARE_GLOBAL_SHADER(FVoxelShadowPassGS);

	FVoxelShadowPassGS() {}
	FVoxelShadowPassGS(const ShaderMetaType::CompiledShaderInitializerType& Initializer) : FVoxelShadowPass(Initializer) { }
};

IMPLEMENT_SHADER_TYPE(, FVoxelShadowPassGS, TEXT("/CustomShaders/VoxelRendering/Voxel_ShadowPass.usf"), TEXT("MainGeometryShader"), SF_Geometry);

class FVoxelShadowPassPS : public FVoxelShadowPass
{
public:

	DECLARE_GLOBAL_SHADER(FVoxelShadowPassPS);

	FVoxelShadowPassPS() {}
	FVoxelShadowPassPS(const ShaderMetaType::CompiledShaderInitializerType& Initializer) : FVoxelShadowPass(Initializer) { }
};

IMPLEMENT_SHADER_TYPE(, FVoxelShadowPassPS, TEXT("/CustomShaders/VoxelRendering/Voxel_ShadowPass.usf"), TEXT("MainPixelShader"), SF_Pixel);

FVoxelManager* FVoxelManager::instance = nullptr;

struct FVertex
{
	FVector4 Position;
};

void FVoxelManager::RegisterVoxelInstances(TArray<MagicaVoxelInstance*> InInstances)
{
	this->Instances = InInstances;
	bRegisteredVoxelInstances = true;
}

void FVoxelManager::RegisterVoxelModels(TArray<VoxelModel> InVoxelModels)
{
	this->VoxelModels = InVoxelModels;
	bRegisteredVoxelInstances = true;
}

void FVoxelManager::RegisterExtension()
{
	static bool bInit = false;
	if (!bInit)
	{
		GEngine->GetDeferredRendererPrePassDelegate().AddRaw(this, &FVoxelManager::RenderPrePass);
		GEngine->GetDeferredRendererBasePassDelegate().AddRaw(this, &FVoxelManager::RenderBasePass);
		GEngine->GetDeferredRendererShadowPassDelegate().AddRaw(this, &FVoxelManager::RenderShadowPass);
		bInit = true;
	}
}

uint32_t FVoxelManager::GetNumberOfInstances()
{
	return NUM_CUBES;
}

uint32_t FVoxelManager::GetNumberOfInstancesX()
{
	return NUM_CUBES_X;
}

uint32_t FVoxelManager::GetNumberOfInstancesY()
{
	return NUM_CUBES_Y;
}

uint32_t FVoxelManager::GetNumberOfInstancesZ()
{
	return NUM_CUBES_Z;
}

void FVoxelManager::SetWorld(UWorld* InWorld)
{
	World = InWorld;
}

void FVoxelManager::AddVoxelModel(VoxelModel Model)
{
	VoxelModels.Add(Model);
}

void FVoxelManager::InitializeBuffers()
{
	if (bInitializedBuffers)
		return;

	TResourceArray<uint32_t, DEFAULT_ALIGNMENT> VoxelsBufferData;

	VoxelsBufferData.SetNumUninitialized(6);

	VoxelsBufferData[0] = 0 << 24;
	VoxelsBufferData[1] = 1 << 24;
	VoxelsBufferData[2] = 2 << 24;
	VoxelsBufferData[3] = 3 << 24;
	VoxelsBufferData[4] = 4 << 24;
	VoxelsBufferData[5] = 5 << 24;

	FRHIResourceCreateInfo CreateInfoVoxels(TEXT("ParticleSandboxVoxelsBuffer"), &VoxelsBufferData);
	VoxelsBuffer = RHICreateStructuredBuffer(sizeof(uint32_t), VoxelsBufferData.GetResourceDataSize(), BUF_Static | BUF_ShaderResource, CreateInfoVoxels);
	VoxelsSRV = RHICreateShaderResourceView(VoxelsBuffer);

	int32 NumVoxels = 6;
	
	if (bRegisteredVoxelInstances)
	{
		for (const MagicaVoxelInstance* const Instance : Instances)
		{
			NumVoxels += Instance->VoxelChunk->NumVoxels;
		}
	}

	FRHIResourceCreateInfo CreateInfoCulledVoxels(TEXT("ParticleSandboxCulledVoxelsBuffer"));
	CulledVoxelsBuffer = RHICreateStructuredBuffer(sizeof(FVoxelInfo), sizeof(FVoxelInfo) * NUM_MAX_VOXELS, BUF_UnorderedAccess | BUF_ShaderResource, CreateInfoCulledVoxels);
	CulledVoxelsSRV = RHICreateShaderResourceView(CulledVoxelsBuffer);
	CulledVoxelsUAV = RHICreateUnorderedAccessView(CulledVoxelsBuffer, false, false);

	std::vector<uint32_t> Colors(NumVoxels);

	Colors[0] = 0x00ff0000;
	Colors[1] = 0x00ffff00;
	Colors[2] = 0x00ff00ff;
	Colors[3] = 0x000000ff;
	Colors[4] = 0x0000ff00;
	Colors[5] = 0x00ffffff;

	TResourceArray<uint32_t, DEFAULT_ALIGNMENT> ColorsBufferData;
	ColorsBufferData.SetNumUninitialized(NumVoxels);

	if (bRegisteredVoxelInstances)
	{
		int32 NumInstance = 0;
		for (const MagicaVoxelInstance* const Instance : Instances)
		{
			for (int32 i = 0; i < Instance->VoxelChunk->NumVoxels; ++i)
			{
				FLinearColor Color = Instance->VoxelChunk->Palette[Instance->VoxelChunk->Voxels[i]->ColorIndex];

				ColorsBufferData[NumInstance] = 0x00ffffff;// Color.R, Color.G, Color.B);
				NumInstance++;
			}
		}
	}
	else
	{
		for (int32 i = 0; i < NumVoxels; ++i)
		{
			ColorsBufferData[i] = Colors[i];
		}
	}

	FRHIResourceCreateInfo CreateInfoCulledVoxelsCounter(TEXT("ParticleSandboxCulledVoxelsCounterBuffer"));
	CulledVoxelsCounterBuffer = RHICreateVertexBuffer(5 * sizeof(uint32), BUF_UnorderedAccess | BUF_DrawIndirect, ERHIAccess::IndirectArgs, CreateInfoCulledVoxelsCounter);
	CulledVoxelsCounterUAV = RHICreateUnorderedAccessView(CulledVoxelsCounterBuffer, PF_R32_UINT);


	FRHIResourceCreateInfo CreateInfoColors(TEXT("ParticleSandboxTestColorsBuffer"), &ColorsBufferData);
	VoxelsColorsBuffer = RHICreateStructuredBuffer(sizeof(uint32_t), ColorsBufferData.GetResourceDataSize(), BUF_Static | BUF_ShaderResource, CreateInfoColors);
	VoxelsColorsSRV = RHICreateShaderResourceView(VoxelsColorsBuffer);

	TResourceArray<FVector, DEFAULT_ALIGNMENT> PositionsBufferData;
	if (bRegisteredVoxelInstances)
	{
		PositionsBufferData.SetNumUninitialized(NumVoxels);
	}
	else
	{
		PositionsBufferData.SetNumUninitialized(NUM_CUBES);
	}
	TResourceArray<FVector4, DEFAULT_ALIGNMENT> RotationsBufferData;
	if (bRegisteredVoxelInstances)
	{
		RotationsBufferData.SetNumUninitialized(NumVoxels);
	}
	else
	{
		RotationsBufferData.SetNumUninitialized(NUM_CUBES);
	}

	if (bRegisteredVoxelInstances)
	{
		int32 NumInstance = 0;
		for (const VoxelModel Model : VoxelModels)
		{
			MagicaVoxelInstance* Instance = Instances[Model.InstanceID];
			MagicaVoxelChunk* VoxelChunk = Instance->VoxelChunk;

			for (int32 i = 0; i < Instance->VoxelChunk->NumVoxels; ++i)
			{
				FVector Position;
				Position.X = Instance->VoxelChunk->Voxels[i]->X;
				Position.Y = Instance->VoxelChunk->Voxels[i]->Y;
				Position.Z = Instance->VoxelChunk->Voxels[i]->Z;
				Position += Model.TransformPosition;

				PositionsBufferData[NumInstance] = Instance->FinalMatrix.TransformPosition(Position);
				RotationsBufferData[NumInstance] = Instance->FinalRotationMatrix.Rotator().Euler();
				NumInstance++;
			}
		}
	}
	else
	{
		for (int z = 0; z < NUM_CUBES_Z; z++)
		{
			for (int y = 0; y < NUM_CUBES_Y; y++)
			{
				for (int x = 0; x < NUM_CUBES_X; x++)
				{
					PositionsBufferData[z * NUM_CUBES_X * NUM_CUBES_Y + y * NUM_CUBES_X + x] = FVector(x * 4, y * 4, z * 4);
					RotationsBufferData[z * NUM_CUBES_X * NUM_CUBES_Y + y * NUM_CUBES_X + x] = FVector4(x, y, z, 1.0f);
				}
			}
		}
	}

	FRHIResourceCreateInfo CreateInfoPositions(TEXT("ParticleSandboxObjectsPositionsBuffer"), &PositionsBufferData);
	PositionsBuffer = RHICreateStructuredBuffer(sizeof(FVector), PositionsBufferData.GetResourceDataSize(), BUF_Static | BUF_UnorderedAccess, CreateInfoPositions);
	PositionsSRV = RHICreateShaderResourceView(PositionsBuffer);
	PositionsUAV = RHICreateUnorderedAccessView(PositionsBuffer, false, false);

	FRHIResourceCreateInfo CreateInfoRotations(TEXT("ParticleSandboxObjectsRotationsBuffer"), &RotationsBufferData);
	RotationsBuffer = RHICreateStructuredBuffer(sizeof(FVector4), RotationsBufferData.GetResourceDataSize(), BUF_Static | BUF_UnorderedAccess, CreateInfoRotations);
	RotationsSRV = RHICreateShaderResourceView(RotationsBuffer);
	RotationsUAV = RHICreateUnorderedAccessView(RotationsBuffer, false, false);

	bInitializedBuffers = true;
}

bool IsMatrixOrthogonal(const FMatrix& Matrix)
{
	const FVector MatrixScale = Matrix.GetScaleVector();

	if (MatrixScale.GetAbsMin() >= KINDA_SMALL_NUMBER)
	{
		FVector AxisX;
		FVector AxisY;
		FVector AxisZ;
		Matrix.GetUnitAxes(AxisX, AxisY, AxisZ);

		return FMath::Abs(AxisX | AxisY) < KINDA_SMALL_NUMBER
			&& FMath::Abs(AxisX | AxisZ) < KINDA_SMALL_NUMBER
			&& FMath::Abs(AxisY | AxisZ) < KINDA_SMALL_NUMBER;
	}

	return false;
}

void FVoxelManager::CullVoxels(FRDGBuilder& GraphBuilder, FViewMatrices& ViewMatrices, FProjectedShadowInfo* ProjectedShadowInfo)
{
	FRHICommandListImmediate& RHICmdList = GetImmediateCommandList_ForRenderCommand();

	//clear indirect draw buffer
	TShaderMapRef<FVoxelCullClearCS> ClearComputeShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));

	FVoxelCullClearCS::FParameters* PassParametersClearCS = GraphBuilder.AllocParameters<FVoxelCullClearCS::FParameters>();
	PassParametersClearCS->OutputVoxelsCounter = CulledVoxelsCounterUAV;

	FComputeShaderUtils::AddPass(
		GraphBuilder,
		RDG_EVENT_NAME("Cull Voxels Clear"),
		ERDGPassFlags::Compute |
		ERDGPassFlags::NeverCull,
		ClearComputeShader,
		PassParametersClearCS,
		FIntVector(1, 1, 1)
	);

	//cull voxels
	TShaderMapRef<FVoxelCullCS> ComputeShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));

	int32 NumVoxels = 6;
	int32 NumInstances = NUM_CUBES;
	if (bRegisteredVoxelInstances)
	{
		for (const MagicaVoxelInstance* const Instance : Instances)
		{
			NumVoxels += Instance->VoxelChunk->NumVoxels;
		}

		NumInstances = 1;
	}

	int32_t NumInstanceKernels = FMath::DivideAndRoundUp(NumInstances, 8);
	int32_t MaxTID = GRHIMaxDispatchThreadGroupsPerDimension.Y;
	int32_t NumCullStages = FMath::DivideAndRoundUp(NumInstanceKernels, MaxTID);

	for (int i = 0; i < NumCullStages; i++)
	{
		FVoxelCullCS::FParameters* PassParametersCS = GraphBuilder.AllocParameters<FVoxelCullCS::FParameters>();
		PassParametersCS->InputVoxels = VoxelsSRV;
		PassParametersCS->InputVoxelsColors = VoxelsColorsSRV;
		PassParametersCS->OutputVoxels = CulledVoxelsUAV;
		PassParametersCS->OutputVoxelsCounter = CulledVoxelsCounterUAV;
		PassParametersCS->Positions = PositionsSRV;
		PassParametersCS->NumInstances = NumInstances;
		PassParametersCS->NumVoxels = NumVoxels;
		PassParametersCS->VoxelScale = VoxelScale;

		if (ProjectedShadowInfo)
		{
			PassParametersCS->ViewOrigin = ViewMatrices.GetViewOrigin() * 100000.0f;
			PassParametersCS->PreViewTranslation = ProjectedShadowInfo->PreShadowTranslation;
			PassParametersCS->ViewProjectionMatrix = ProjectedShadowInfo->TranslatedWorldToClipOuterMatrix;
		}
		else
		{
			PassParametersCS->ViewOrigin = ViewMatrices.GetViewOrigin();
			PassParametersCS->PreViewTranslation = ViewMatrices.GetPreViewTranslation();
			PassParametersCS->ViewProjectionMatrix = ViewMatrices.GetTranslatedViewProjectionMatrix();
		}

		PassParametersCS->InstanceOffset = 8 * i * MaxTID;

		int32_t InstancesCount = MaxTID;
		if(i == NumCullStages - 1)
			InstancesCount = (NumInstanceKernels - PassParametersCS->InstanceOffset) % (MaxTID + 1);

		FComputeShaderUtils::AddPass(
			GraphBuilder,
			RDG_EVENT_NAME("Cull Voxels"),
			ERDGPassFlags::Compute |
			ERDGPassFlags::NeverCull,
			ComputeShader,
			PassParametersCS,
			FIntVector(FMath::DivideAndRoundUp(NumVoxels, 8), InstancesCount, 1)
		);
	}
}

void FVoxelManager::RenderPrePass(FRDGBuilder& GraphBuilder, FViewInfo& View)
{
	check(IsInRenderingThread());

	InitializeBuffers();

	CullVoxels(GraphBuilder, View.ViewMatrices);

	TShaderMapRef<FVoxelBasePassVS> VertexShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));
	TShaderMapRef<FVoxelBasePassGS> GeometryShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));
	TShaderMapRef<FVoxelBasePassPS> PixelShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));
	auto ShaderMap = GetGlobalShaderMap(GMaxRHIFeatureLevel);

	FVoxelBasePass::FParameters* PassParameters = GraphBuilder.AllocParameters<FVoxelBasePass::FParameters>();
	const FSceneTextures& SceneTextures = FSceneTextures::Get(GraphBuilder);

	FRDGTextureMSAA SceneDepthTexture = SceneTextures.Depth;
	PassParameters->RenderTargets.DepthStencil = FDepthStencilBinding(SceneDepthTexture.Target, ERenderTargetLoadAction::ELoad, ERenderTargetLoadAction::ELoad, FExclusiveDepthStencil::DepthWrite_StencilNop);
	PassParameters->Voxels = CulledVoxelsSRV;
	PassParameters->Positions = PositionsSRV;
	PassParameters->Rotations = RotationsSRV;
	PassParameters->ViewProjectionMatrix = View.ViewMatrices.GetTranslatedViewProjectionMatrix();
	PassParameters->PreViewTranslation = View.ViewMatrices.GetPreViewTranslation();
	PassParameters->VoxelScale = VoxelScale;

	GraphBuilder.AddPass(
		RDG_EVENT_NAME("BFS Pre Pass"),
		PassParameters,
		ERDGPassFlags::Raster,
		[this, VertexShader, GeometryShader, PixelShader, PassParameters](FRHICommandListImmediate& RHICmdList)
		{
			FGraphicsPipelineStateInitializer GraphicsPSOInit;
			RHICmdList.ApplyCachedRenderTargets(GraphicsPSOInit);
			GraphicsPSOInit.DepthStencilState = TStaticDepthStencilState<true, CF_GreaterEqual>::GetRHI();
			GraphicsPSOInit.BlendState = TStaticBlendState<>::GetRHI();
			GraphicsPSOInit.RasterizerState = TStaticRasterizerState<FM_Solid, CM_CW>::GetRHI();
			GraphicsPSOInit.PrimitiveType = PT_PointList;
			GraphicsPSOInit.BoundShaderState.VertexDeclarationRHI = GetVertexDeclarationFVector4();
			GraphicsPSOInit.BoundShaderState.VertexShaderRHI = VertexShader.GetVertexShader();
			GraphicsPSOInit.BoundShaderState.GeometryShaderRHI = GeometryShader.GetGeometryShader();
			GraphicsPSOInit.BoundShaderState.PixelShaderRHI = PixelShader.GetPixelShader();
			SetGraphicsPipelineState(RHICmdList, GraphicsPSOInit);

			SetShaderParameters(RHICmdList, VertexShader, VertexShader.GetVertexShader(), *PassParameters);
			SetShaderParameters(RHICmdList, GeometryShader, GeometryShader.GetGeometryShader(), *PassParameters);
			SetShaderParameters(RHICmdList, PixelShader, PixelShader.GetPixelShader(), *PassParameters);

			//RHICmdList.SetStreamSource()
			RHICmdList.DrawPrimitiveIndirect(CulledVoxelsCounterBuffer, 0);
		}
	);
}

void FVoxelManager::RenderBasePass(FRDGBuilder& GraphBuilder, FViewInfo& View)
{
	check(IsInRenderingThread());

	InitializeBuffers();

	CullVoxels(GraphBuilder, View.ViewMatrices);

	TShaderMapRef<FVoxelBasePassVS> VertexShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));
	TShaderMapRef<FVoxelBasePassGS> GeometryShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));
	TShaderMapRef<FVoxelBasePassPS> PixelShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));
	auto ShaderMap = GetGlobalShaderMap(GMaxRHIFeatureLevel);

	FVoxelBasePass::FParameters* PassParameters = GraphBuilder.AllocParameters<FVoxelBasePass::FParameters>();

	const FSceneTextures& SceneTextures = FSceneTextures::Get(GraphBuilder);
	
	const FRDGTextureRef GBufferATexture = SceneTextures.GBufferA;
	const FRDGTextureRef GBufferBTexture = SceneTextures.GBufferB;
	const FRDGTextureRef GBufferCTexture = SceneTextures.GBufferC;
	const FRDGTextureRef GBufferDTexture = SceneTextures.GBufferD;
	const FRDGTextureRef GBufferETexture = SceneTextures.GBufferE;

	FRDGTextureMSAA SceneDepthTexture = SceneTextures.Depth;
	PassParameters->RenderTargets[0] = FRenderTargetBinding(GBufferATexture, ERenderTargetLoadAction::ELoad);
	PassParameters->RenderTargets[1] = FRenderTargetBinding(GBufferBTexture, ERenderTargetLoadAction::ELoad);
	PassParameters->RenderTargets[2] = FRenderTargetBinding(GBufferCTexture, ERenderTargetLoadAction::ELoad);
	PassParameters->RenderTargets[3] = FRenderTargetBinding(GBufferDTexture, ERenderTargetLoadAction::ELoad);
	PassParameters->RenderTargets[4] = FRenderTargetBinding(GBufferETexture, ERenderTargetLoadAction::ELoad);

	PassParameters->RenderTargets.DepthStencil = FDepthStencilBinding(SceneDepthTexture.Target, ERenderTargetLoadAction::ELoad, ERenderTargetLoadAction::ENoAction, FExclusiveDepthStencil::DepthWrite_StencilNop);

	PassParameters->Voxels = CulledVoxelsSRV;
	PassParameters->Positions = PositionsSRV;
	PassParameters->Rotations = RotationsSRV;
	PassParameters->ViewProjectionMatrix = View.ViewMatrices.GetTranslatedViewProjectionMatrix();
	PassParameters->PreViewTranslation = View.ViewMatrices.GetPreViewTranslation();
	PassParameters->VoxelScale = VoxelScale;

	GraphBuilder.AddPass(
		RDG_EVENT_NAME("BFS Base Pass"),
		PassParameters,
		ERDGPassFlags::Raster,
		[this, VertexShader, GeometryShader, PixelShader, PassParameters](FRHICommandListImmediate& RHICmdList)
		{
			FGraphicsPipelineStateInitializer GraphicsPSOInit;
			RHICmdList.ApplyCachedRenderTargets(GraphicsPSOInit);
			GraphicsPSOInit.DepthStencilState = TStaticDepthStencilState<true, CF_GreaterEqual>::GetRHI();
			GraphicsPSOInit.BlendState = TStaticBlendState<>::GetRHI();
			GraphicsPSOInit.RasterizerState = TStaticRasterizerState<FM_Solid, CM_CW>::GetRHI();
			GraphicsPSOInit.PrimitiveType = PT_PointList;
			GraphicsPSOInit.BoundShaderState.VertexDeclarationRHI = GetVertexDeclarationFVector4();
			GraphicsPSOInit.BoundShaderState.VertexShaderRHI = VertexShader.GetVertexShader();
			GraphicsPSOInit.BoundShaderState.GeometryShaderRHI = GeometryShader.GetGeometryShader();
			GraphicsPSOInit.BoundShaderState.PixelShaderRHI = PixelShader.GetPixelShader();
			SetGraphicsPipelineState(RHICmdList, GraphicsPSOInit);

			SetShaderParameters(RHICmdList, VertexShader, VertexShader.GetVertexShader(), *PassParameters);
			SetShaderParameters(RHICmdList, GeometryShader, GeometryShader.GetGeometryShader(), *PassParameters);
			SetShaderParameters(RHICmdList, PixelShader, PixelShader.GetPixelShader(), *PassParameters);

			RHICmdList.DrawPrimitiveIndirect(CulledVoxelsCounterBuffer, 0);
		}
	);
}

void FVoxelManager::RenderShadowPass(FRDGBuilder &GraphBuilder, FProjectedShadowInfo* ProjectedShadowInfo, FShadowDepthPassUniformParameters& ShadowDepthPassParameters, FRDGTexture *DepthTexture)
{
	check(IsInRenderingThread());

	InitializeBuffers();


	FViewMatrices::FMinimalInitializer MatricesInitializer;
	MatricesInitializer.ViewOrigin = -ProjectedShadowInfo->PreShadowTranslation;
	MatricesInitializer.ConstrainedViewRect = ProjectedShadowInfo->GetOuterViewRect();

	MatricesInitializer.ViewRotationMatrix = ProjectedShadowInfo->TranslatedWorldToView;
	MatricesInitializer.ProjectionMatrix = ProjectedShadowInfo->ViewToClipOuter;

	auto ViewMatrices = FViewMatrices(MatricesInitializer);

	CullVoxels(GraphBuilder, ViewMatrices, ProjectedShadowInfo);

	FVoxelShadowPass::FParameters* PassParameters = GraphBuilder.AllocParameters<FVoxelShadowPass::FParameters>();

	PassParameters->ViewProjectionMatrix = ProjectedShadowInfo->TranslatedWorldToClipOuterMatrix;//ViewMatrices.GetTranslatedViewProjectionMatrix();
	PassParameters->ShadowMatrix = ProjectedShadowInfo->TranslatedWorldToView;//ViewMatrices.GetTranslatedViewMatrix().GetTransposed();

	PassParameters->PreViewTranslation = ProjectedShadowInfo->PreShadowTranslation;////ViewMatrices.GetPreViewTranslation();
	PassParameters->ShadowParams = ShadowDepthPassParameters.ShadowParams;
	PassParameters->bClampToNearPlane = ShadowDepthPassParameters.bClampToNearPlane;
	PassParameters->VoxelScale = VoxelScale;
	PassParameters->Voxels = CulledVoxelsSRV;
	PassParameters->Positions = PositionsSRV;
	PassParameters->Rotations = RotationsSRV;

	PassParameters->RenderTargets.DepthStencil = FDepthStencilBinding(DepthTexture, ERenderTargetLoadAction::ELoad, ERenderTargetLoadAction::ENoAction, FExclusiveDepthStencil::DepthWrite_StencilNop);

	TShaderMapRef<FVoxelShadowPassVS> VertexShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));
	TShaderMapRef<FVoxelShadowPassGS> GeometryShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));
	TShaderMapRef<FVoxelShadowPassPS> PixelShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));

	GraphBuilder.AddPass(
		RDG_EVENT_NAME("BFS Shadow Pass"),
		PassParameters,
		ERDGPassFlags::Raster,
		[this, ProjectedShadowInfo, VertexShader, GeometryShader, PixelShader, PassParameters](FRHICommandListImmediate& RHICmdList)
		{
			RHICmdList.SetViewport(
				ProjectedShadowInfo->X + ProjectedShadowInfo->BorderSize,
				ProjectedShadowInfo->Y + ProjectedShadowInfo->BorderSize,
				0.0f,
				ProjectedShadowInfo->X + ProjectedShadowInfo->BorderSize + ProjectedShadowInfo->ResolutionX,
				ProjectedShadowInfo->Y + ProjectedShadowInfo->BorderSize + ProjectedShadowInfo->ResolutionY,
				1.0f
			);

			FGraphicsPipelineStateInitializer GraphicsPSOInit;
			RHICmdList.ApplyCachedRenderTargets(GraphicsPSOInit);
			GraphicsPSOInit.DepthStencilState = TStaticDepthStencilState<true, CF_LessEqual>::GetRHI();
			GraphicsPSOInit.BlendState = TStaticBlendState<>::GetRHI();
			GraphicsPSOInit.RasterizerState = TStaticRasterizerState<FM_Solid, CM_CW>::GetRHI();
			GraphicsPSOInit.PrimitiveType = PT_PointList;
			GraphicsPSOInit.BoundShaderState.VertexDeclarationRHI = GetVertexDeclarationFVector4();
			GraphicsPSOInit.BoundShaderState.VertexShaderRHI = VertexShader.GetVertexShader();
			GraphicsPSOInit.BoundShaderState.GeometryShaderRHI = GeometryShader.GetGeometryShader();
			GraphicsPSOInit.BoundShaderState.PixelShaderRHI = PixelShader.GetPixelShader();
			SetGraphicsPipelineState(RHICmdList, GraphicsPSOInit);

			SetShaderParameters(RHICmdList, VertexShader, VertexShader.GetVertexShader(), *PassParameters);
			SetShaderParameters(RHICmdList, GeometryShader, GeometryShader.GetGeometryShader(), *PassParameters);
			SetShaderParameters(RHICmdList, PixelShader, PixelShader.GetPixelShader(), *PassParameters);

			RHICmdList.DrawPrimitiveIndirect(CulledVoxelsCounterBuffer, 0);
		}
	);
}
