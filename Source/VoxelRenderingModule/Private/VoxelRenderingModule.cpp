#include "../Public/VoxelRenderingModule.h"


void FVoxelRenderingModule::StartupModule()
{
	UE_LOG(LogTemp, Log, TEXT("Rendering module has started!"));
	FString ShaderDirectory = FPaths::Combine(FPaths::ProjectDir(), TEXT("Source/Shaders"));
	AddShaderSourceDirectoryMapping("/CustomShaders", ShaderDirectory);
}

void FVoxelRenderingModule::ShutdownModule()
{
	UE_LOG(LogTemp, Log, TEXT("Rendering module has shutdown!"));
}


IMPLEMENT_MODULE(FVoxelRenderingModule, VoxelRenderingModule)