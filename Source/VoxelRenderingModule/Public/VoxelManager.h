// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GlobalShader.h"
#include "ShaderParameterStruct.h"
#include "RenderGraphUtils.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"
#include "SceneRendering.h"
#include "ShaderBaseClasses.h"
#include "ShadowRendering.h"
#include "MagicaVoxelImporter.h"
#include "VoxelModel.h"

/**
 *
 */
class VOXELRENDERINGMODULE_API FVoxelManager
{
private:

	static FVoxelManager* instance;

	UWorld* World = nullptr;

	bool bInitializedBuffers = false;
	void InitializeBuffers();

	bool bRegisteredVoxelInstances = false;
	
	FBufferRHIRef				PositionsBuffer = nullptr;
	FShaderResourceViewRHIRef	PositionsSRV = nullptr;
	FUnorderedAccessViewRHIRef	PositionsUAV = nullptr;

	FBufferRHIRef				RotationsBuffer = nullptr;
	FShaderResourceViewRHIRef	RotationsSRV = nullptr;
	FUnorderedAccessViewRHIRef	RotationsUAV = nullptr;

	FBufferRHIRef				VoxelsBuffer = nullptr;
	FShaderResourceViewRHIRef	VoxelsSRV = nullptr;

	FBufferRHIRef				VoxelsColorsBuffer = nullptr;
	FShaderResourceViewRHIRef	VoxelsColorsSRV = nullptr;

	FBufferRHIRef				CulledVoxelsBuffer = nullptr;
	FShaderResourceViewRHIRef	CulledVoxelsSRV = nullptr;
	FUnorderedAccessViewRHIRef	CulledVoxelsUAV = nullptr;

	FBufferRHIRef				CulledVoxelsCounterBuffer = nullptr;
	FUnorderedAccessViewRHIRef	CulledVoxelsCounterUAV = nullptr;

	float						VoxelScale = 25.0f;

	TArray<MagicaVoxelInstance*>	Instances;
	TArray<VoxelModel>				VoxelModels;

	void CullVoxels(FRDGBuilder& GraphBuilder, FViewMatrices& ViewMatrices,  FProjectedShadowInfo* ProjectedShadowInfo = nullptr);
public:

	FVoxelManager() {};

	static FVoxelManager* Get()
	{
		if (!instance)
			instance = new FVoxelManager();
		return instance;
	}

	void RegisterVoxelInstances(TArray<MagicaVoxelInstance*> InInstances);
	void RegisterVoxelModels(TArray<VoxelModel> InVoxelModels);

	void AddVoxelModel(VoxelModel Model);
	void SetWorld(UWorld*);
	void RegisterExtension();

	void RenderPrePass(FRDGBuilder& GraphBuilder, FViewInfo& View);
	void RenderBasePass(FRDGBuilder &GraphBuilder, FViewInfo& View);
	void RenderShadowPass(FRDGBuilder& GraphBuilder, FProjectedShadowInfo* ProjectedShadowInfo, FShadowDepthPassUniformParameters& ShadowDepthPassParameters, FRDGTexture* DepthTexture);

	uint32_t GetNumberOfInstances();
	uint32_t GetNumberOfInstancesX();
	uint32_t GetNumberOfInstancesY();
	uint32_t GetNumberOfInstancesZ();
	FUnorderedAccessViewRHIRef GetPositionsUAV() { return PositionsUAV; }
	FUnorderedAccessViewRHIRef GetRotationsUAV() { return RotationsUAV; }
};
