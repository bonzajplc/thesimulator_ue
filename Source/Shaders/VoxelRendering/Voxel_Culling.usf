#include "/Engine/Public/Platform.ush"
#include "Voxel_Common.ush"

#include "/Engine/Private/Nanite/HZBCull.ush"

StructuredBuffer<uint> InputVoxels;
//StructuredBuffer<uint> InputVoxelsData;
StructuredBuffer<uint> InputVoxelsColors;
RWStructuredBuffer<uint> OutputVoxelsCounter;
RWStructuredBuffer<VoxelInfo> OutputVoxels;

float3 ViewOrigin;

uint NumVoxels;
uint NumInstances;
uint InstanceOffset;

[numthreads(1, 1, 1)]
void VoxelCullClearCS(uint DIT : SV_DispatchThreadID)
{
    OutputVoxelsCounter[0] = 0; //increment vertex (voxel) count while culling
    OutputVoxelsCounter[1] = 1; //Num Instances
    OutputVoxelsCounter[2] = 0;
    OutputVoxelsCounter[3] = 0;
    OutputVoxelsCounter[4] = 0;
}

[numthreads(8, 8, 1)]
void VoxelCullCS(uint2 VoxelInstanceIndex : SV_DispatchThreadID)
{
    uint VoxelID = VoxelInstanceIndex.x;
    uint InstanceID = InstanceOffset + VoxelInstanceIndex.y;
    if (VoxelID >= NumVoxels || InstanceID >= NumInstances)
        return;

    uint positionAndFace = InputVoxels[VoxelInstanceIndex.x];
    uint faceConfig = positionAndFace >> 24;
       
    float3 positionWS = float3(positionAndFace & 0xFF, (positionAndFace >> 8) & 0xFF, (positionAndFace >> 16) & 0xFF) + Positions[InstanceID] + normals[faceConfig];
    positionWS *= VoxelScale;

    float3 ViewVector = normalize(positionWS - ViewOrigin);
 
    //backface culling
    float3 normalWS = normals[faceConfig];

    if (dot(-ViewVector, normalWS) <= 0.0f)
        return;

	//frustum culling
    float3 Extent = VoxelScale;
    
    positionWS += PreViewTranslation;
    FFrustumCullData CullData = BoxCullFrustumGeneral(positionWS, Extent, ViewProjectionMatrix, 0.1f, false);
    
    if (!CullData.bIsVisible)
        return;
    
    
    VoxelInfo output;
    output.VoxelPosXYZ_Face = positionAndFace;
    output.VoxelSizeWH_MaterialData = 0;//    InputVoxelsData[VoxelInstanceIndex.x];
    output.VoxelColorRGBA = InputVoxelsColors[ VoxelID];
    output.InstanceID = InstanceID;
    
    int VoxelAddress;
    InterlockedAdd(OutputVoxelsCounter[0], 1, VoxelAddress);
    
    //int orig;
    //InterlockedMin(OutputVoxelsCounter[0], (4 * 1024 * 1024), orig);
    
    if (VoxelAddress < (4 * 1024 * 1024))
        OutputVoxels[VoxelAddress] = output;
    else
        OutputVoxelsCounter[0] = 4 * 1024 * 1024;

}
