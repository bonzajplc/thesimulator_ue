using UnrealBuildTool;
using System.IO;

public class SimulationModule : ModuleRules
{
    public SimulationModule(ReadOnlyTargetRules Target) : base(Target)
    {
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[]
        {
            "Core",
            "CoreUObject",
            "Engine",
            "RenderCore",
            "RHI",
            "Renderer",
            "VoxelRenderingModule"
        });

        string EnginePath = Path.GetFullPath(Target.RelativeEnginePath);
        string SrcRuntimePath = EnginePath + "Source/Runtime/";
        PublicIncludePaths.AddRange(new string[] {SrcRuntimePath + "Renderer/Private"});
        PrivateIncludePaths.AddRange(new string[] { SrcRuntimePath + "Renderer/Private"});
    }
}