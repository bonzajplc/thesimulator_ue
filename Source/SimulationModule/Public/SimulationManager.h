// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

#include "GlobalShader.h"
#include "ShaderParameterStruct.h"
#include "RenderGraphUtils.h"
#include "Modules/ModuleManager.h"
#include "Modules/ModuleInterface.h"

/**
 *
 */
class SIMULATIONMODULE_API FSimulationManager
{
private:
	static FSimulationManager* instance;

	UWorld* World = nullptr;

	void DispatchComputeShaders(FRDGBuilder& GraphBuilder);

public:
	FSimulationManager() {};

	static FSimulationManager* Get()
	{
		if (!instance)
			instance = new FSimulationManager();
		return instance;
	}

	void RegisterExtension();

	void SetWorld(UWorld*);
};
