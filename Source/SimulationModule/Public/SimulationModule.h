#pragma once

#include "CoreMinimal.h"

class FSimulationModule : public IModuleInterface
{
	public:

		virtual void StartupModule() override;
		virtual void ShutdownModule() override;
};