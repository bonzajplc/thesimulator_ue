#include "../Public/SimulationModule.h"


void FSimulationModule::StartupModule()
{
	UE_LOG(LogTemp, Log, TEXT("Rendering module has started!"));
	FString ShaderDirectory = FPaths::Combine(FPaths::ProjectDir(), TEXT("Source/Shaders"));
	AddShaderSourceDirectoryMapping("/CustomShaders", ShaderDirectory);
}

void FSimulationModule::ShutdownModule()
{
	UE_LOG(LogTemp, Log, TEXT("Simulation module has shutdown!"));
}


IMPLEMENT_MODULE(FSimulationModule, SimulationModule)