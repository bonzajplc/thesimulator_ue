// Fill out your copyright notice in the Description page of Project Settings.


#include "SimulationManager.h"

#include "VoxelManager.h"

FSimulationManager* FSimulationManager::instance = nullptr;

class FTransformInstancesCS : public FGlobalShader
{
private:

	DECLARE_GLOBAL_SHADER(FTransformInstancesCS);
	SHADER_USE_PARAMETER_STRUCT(FTransformInstancesCS, FGlobalShader);

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_UAV(RWStructuredBuffer<float3>, Positions)
		SHADER_PARAMETER_UAV(RWStructuredBuffer<float4>, Rotations)
		SHADER_PARAMETER(FVector, NumInstances)
		SHADER_PARAMETER(float, Time)
		END_SHADER_PARAMETER_STRUCT()

public:

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static bool ShouldCache(EShaderPlatform Platform)
	{
		return true;
	}

	static inline void
		ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters,
			FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);

		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_X"), 256);
		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_Y"), 1);
		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_Z"), 1);
	}
};

IMPLEMENT_SHADER_TYPE(, FTransformInstancesCS, TEXT("/CustomShaders/Simulation/Transform_Instances.usf"), TEXT("TransformInstancesCS"), SF_Compute);

void FSimulationManager::RegisterExtension()
{
	GEngine->GetPreRenderDelegateEx().AddRaw(this, &FSimulationManager::DispatchComputeShaders);
}

void FSimulationManager::SetWorld(UWorld* InWorld)
{
	World = InWorld;
}

void FSimulationManager::DispatchComputeShaders(FRDGBuilder& GraphBuilder)
{
	//clear indirect draw buffer
	TShaderMapRef<FTransformInstancesCS> ComputeShader(GetGlobalShaderMap(GMaxRHIFeatureLevel));

	FTransformInstancesCS::FParameters* PassParametersCS = GraphBuilder.AllocParameters<FTransformInstancesCS::FParameters>();
	PassParametersCS->Positions = FVoxelManager::Get()->GetPositionsUAV();
	PassParametersCS->Rotations = FVoxelManager::Get()->GetRotationsUAV();
	PassParametersCS->NumInstances = FVector(FVoxelManager::Get()->GetNumberOfInstancesX(), FVoxelManager::Get()->GetNumberOfInstancesY(), FVoxelManager::Get()->GetNumberOfInstancesZ());
	PassParametersCS->Time = World->GetTimeSeconds();


	if (!PassParametersCS->Positions || !PassParametersCS->Rotations)
		return;

	FComputeShaderUtils::AddPass(
		GraphBuilder,
		RDG_EVENT_NAME("Transform Instances"),
		ERDGPassFlags::Compute |
		ERDGPassFlags::NeverCull,
		ComputeShader,
		PassParametersCS,
		FIntVector(FMath::DivideAndRoundUp((int)FVoxelManager::Get()->GetNumberOfInstancesX(), 8), FMath::DivideAndRoundUp((int)FVoxelManager::Get()->GetNumberOfInstancesY(), 8), FMath::DivideAndRoundUp((int)FVoxelManager::Get()->GetNumberOfInstancesZ(), 8))
	);
}

