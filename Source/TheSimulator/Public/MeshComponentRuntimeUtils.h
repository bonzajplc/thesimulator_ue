#pragma once

#include "CoreMinimal.h"
#include "Engine/StaticMesh.h"
#include "DynamicMesh3.h"

namespace RTGUtils
{
	/**
	 * Reinitialize the given StaticMesh with the input FDynamicMesh3.
	 * This calls StaticMesh->BuildFromMeshDescriptions(), which can be used at Runtime (vs StaticMesh->Build() which cannot)
	 */
	THESIMULATOR_API void UpdateStaticMeshFromDynamicMesh(
		UStaticMesh* StaticMesh,
		const FDynamicMesh3* Mesh);
}
