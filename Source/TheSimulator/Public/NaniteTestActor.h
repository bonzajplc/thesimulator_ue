// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "DynamicMesh3.h"
#include "Components/StaticMeshComponent.h"

#include "NaniteTestActor.generated.h"

/**
 * Type of Normals computation used by ADynamicMeshBaseActor
 */
UENUM()
enum class EDynamicMeshActorNormalsMode : uint8
{
	SplitNormals = 0,
	PerVertexNormals = 1,
	FaceNormals = 2
};

/**
 * Source of mesh used to initialize ADynamicMeshBaseActor
 */
UENUM()
enum class EDynamicMeshActorSourceType : uint8
{
	Primitive,
	ImportedMesh,
	/** Do not Initialize the mesh, allow an external source (eg a Construction Script) to initialize it */
	ExternallyGenerated
};

UCLASS()
class THESIMULATOR_API ANaniteTestActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ANaniteTestActor();

public:

	UPROPERTY(EditAnywhere, Category = MeshOptions)
	EDynamicMeshActorSourceType SourceType = EDynamicMeshActorSourceType::ExternallyGenerated;

	UPROPERTY(EditAnywhere, Category = MeshOptions)
	EDynamicMeshActorNormalsMode NormalsMode = EDynamicMeshActorNormalsMode::SplitNormals;

	UPROPERTY(EditAnywhere, Category = MaterialOptions)
	UMaterialInterface* Material;

	UPROPERTY(EditAnywhere, Category = PrimitiveOptions, meta = (EditCondition = "SourceType == EDynamicMeshSourceType::Primitive", EditConditionHides))
	bool bRegenerateOnTick = false;

	/**
	 * Call EditMesh() to safely modify the SourceMesh owned by this Actor.
	 * Your EditFunc will be called with the Current SourceMesh as argument,
	 * and you are expected to pass back the new/modified version.
	 * (If you are generating an entirely new mesh, MoveTemp can be used to do this without a copy)
	 */
	virtual void EditMesh(TFunctionRef<void(FDynamicMesh3&)> EditFunc);

	/**
	 * Get a copy of the current SourceMesh stored in MeshOut
	 */
	virtual void GetMeshCopy(FDynamicMesh3& MeshOut);

	/**
	 * Get a reference to the current SourceMesh
	 */
	virtual const FDynamicMesh3& GetMeshRef() const;

	/**
	 * This delegate is broadcast whenever the internal SourceMesh is updated
	 */
	DECLARE_MULTICAST_DELEGATE_OneParam(FOnMeshModified, ANaniteTestActor*);
	FOnMeshModified OnMeshModified;

protected:

	FDynamicMesh3 SourceMesh;

	/** Accumulated time since Actor was created, this is used for the animated primitives when bRegenerateOnTick = true*/
	double AccumulatedTime = 0;

	/** Called whenever the initial Source mesh needs to be regenerated / re-imported. Calls EditMesh() to do so. */
	virtual void OnMeshGenerationSettingsModified();

	/** Called to generate or import a new source mesh. Override this to provide your own generated mesh. */
	virtual void RegenerateSourceMesh(FDynamicMesh3& MeshOut);

	/** Call this on a Mesh to compute normals according to the NormalsMode setting */
	virtual void RecomputeNormals(FDynamicMesh3& MeshOut);

	//
	// ADynamicMeshBaseActor API that subclasses must implement.
	//
protected:
	/**
	 * Called when the SourceMesh has been modified. Subclasses override this function to
	 * update their respective Component with the new SourceMesh.
	 */
	virtual void OnMeshEditedInternal();



	//
	// Standard UE4 Actor Callbacks. If you need to override these functions,
	// make sure to call (eg) Super::Tick() or you will break the mesh updating functionality.
	//
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void PostLoad() override;
	virtual void PostActorCreated() override;

#if WITH_EDITOR
	// called when property is modified. This will call OnMeshGenerationSettingsModified() to update the mesh
	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
#endif

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	/** @return number of triangles in current SourceMesh */
	UFUNCTION(BlueprintCallable)
	int GetTriangleCount();
};

UCLASS()
class THESIMULATOR_API ADynamicSMCActor : public ANaniteTestActor
{
	GENERATED_BODY()
	
public:

	ADynamicSMCActor();

	UPROPERTY(VisibleAnywhere)
	UStaticMeshComponent* MeshComponent = nullptr;

	UPROPERTY(Transient)
	UStaticMesh* StaticMesh = nullptr;

	void BuildNaniteData();

protected:

	virtual void BeginPlay() override;
	virtual void PostLoad() override;
	virtual void PostActorCreated() override;

public:

	virtual void Tick(float DeltaTime) override;

protected:
	
	// ANaniteTestActor API
	virtual void OnMeshEditedInternal() override;

protected:

	virtual void UpdateSMCMesh();
};
