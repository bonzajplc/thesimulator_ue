// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GlobalShader.h"
#include "ShaderParameterStruct.h"
#include "RenderGraphUtils.h"
#include "RHICommandList.h"
#include "GPUHelpers.h"
#include "Generators/MeshShapeGenerator.h"

/**
 *
 */
class THESIMULATOR_API VoxelMeshGenerator : public FMeshShapeGenerator
{
public:
	/** If true, each quad of box gets a separate polygroup */
	bool bPolygroupPerQuad = false;

	float VoxelScale = 25.0f; // voxel scale in cm

private:

	bool bIsDirty = false;
	bool bIsMeshPrepared = false;

	TArray<FVector3f> Centers;

	TResourceArray<FVector3f> CentersBufferData;
	TResourceArray<FVector3f> VerticesBufferData;
	TResourceArray<FVector3f> NormalsBufferData;
	TResourceArray<FIndex3i> TrianglesBufferData;
	TResourceArray<FIndex3i> TriangleUVsBufferData;
	TResourceArray<FIndex3i> TriangleNormalsBufferData;
	TResourceArray<int> TrianglePolygonIDsBufferData;
	TResourceArray<int> NormalParentVertexBufferData;

	GPUData CentersGPU;
	GPUData VerticesGPU;
	GPUData NormalsGPU;
	GPUData TrianglesGPU;
	GPUData TriangleUVsGPU;
	GPUData TriangleNormalsGPU;
	GPUData TrianglePolygonIDsGPU;
	GPUData NormalParentVertexGPU;

public:

	void SetVoxels(TArray<FVector3f> VoxelsCenters)
	{
		Centers = VoxelsCenters;
		bIsDirty = true;
		bIsMeshPrepared = false;
	}

	void Initialize();

	void PrepareMesh(FRDGBuilder& GraphBuilder, FViewInfo& View);
	void OutputTriangles(FRDGBuilder& GraphBuilder, FViewInfo& View);
	void Readback(FRDGBuilder& GraphBuilder, FViewInfo& View);

	bool IsMeshPrepared()
	{
		return bIsMeshPrepared;
	}

	/** Generate the mesh */
	/* Call ONLY if IsMeshPrepared returns true */
	FMeshShapeGenerator& Generate() override
	{
		return *this;
	}
};

