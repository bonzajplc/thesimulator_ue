// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TheSimulatorGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class THESIMULATOR_API ATheSimulatorGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
