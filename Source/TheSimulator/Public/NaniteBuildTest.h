// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "VoxelMeshGenerator.h"
#include "GameFramework/Actor.h"
#include "NaniteTestActor.h"
#include "NaniteBuildTest.generated.h"


UCLASS()
class THESIMULATOR_API ANaniteBuildTest : public AActor
{
	GENERATED_BODY()
	
public:	

	UPROPERTY(EditAnywhere)
	ADynamicSMCActor* SMCTestActor;

	// Sets default values for this actor's properties
	ANaniteBuildTest();

private:

	bool bIsMeshCreated = false;

	VoxelMeshGenerator VoxelGen;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
