// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ConsoleCommandsComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class THESIMULATOR_API UConsoleCommandsComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UConsoleCommandsComponent();

	UPROPERTY(EditAnywhere)
	bool bUnlimitedFPS = true;

	UPROPERTY(EditAnywhere)
	bool bShowFPS = true;

	UPROPERTY(EditAnywhere)
	bool bShowFrameBudget = true;

	UPROPERTY(EditAnywhere)
	bool bShowGPUStats = false;

	UPROPERTY(EditAnywhere)
	bool bShowNaniteStats = false;

private:

	static bool bSetUnlimitedFPS;
	static bool bShownFPS;
	static bool bShownFrameBudget;
	static bool bShownGPUStats;
	static bool bShownNaniteStats;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};
