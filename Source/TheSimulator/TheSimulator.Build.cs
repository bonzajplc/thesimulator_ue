// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class TheSimulator: ModuleRules
{
	public TheSimulator(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
	
		PublicDependencyModuleNames.AddRange(new string[] {
			"Core",
			"CoreUObject",
			"Engine",
			"InputCore",
			"RenderCore",
			"RHI",
			"Renderer",
			"DynamicMesh",
			"MeshConversion",
			"MeshDescription",
            "StaticMeshDescription",
            "MagicaVoxelModule",
			"SimulationModule",
            "VoxelRenderingModule" });

		PrivateDependencyModuleNames.AddRange(new string[] { });

		// Uncomment if you are using Slate UI
		// PrivateDependencyModuleNames.AddRange(new string[] { "Slate", "SlateCore" });
		
		// Uncomment if you are using online features
		// PrivateDependencyModuleNames.Add("OnlineSubsystem");

		// To include OnlineSubsystemSteam, add it to the plugins section in your uproject file with the Enabled attribute set to true
	}
}
