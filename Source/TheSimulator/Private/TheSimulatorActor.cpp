// Fill out your copyright notice in the Description page of Project Settings.


#include "TheSimulatorActor.h"
#include "VoxelManager.h"
#include "SimulationManager.h"
#include "DrawDebugHelpers.h"



// Sets default values
ATheSimulatorActor::ATheSimulatorActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATheSimulatorActor::BeginPlay()
{
	Super::BeginPlay();
	
	FString voxPath = FPaths::ProjectContentDir() + "/Models/chessboard.vox";
	EMagicaVoxelErrorCodes ErrorCode = MagicaVoxelImporter::LoadVOX(voxPath, true, Instances, VoxelModels);
	if (ErrorCode == EMagicaVoxelErrorCodes::NoError)
	{
		FVoxelManager::Get()->RegisterVoxelInstances(Instances);
		FVoxelManager::Get()->RegisterVoxelModels(VoxelModels);
	}
	FVoxelManager::Get()->RegisterExtension();
	FVoxelManager::Get()->SetWorld(GetWorld());

	FSimulationManager::Get()->RegisterExtension();
	FSimulationManager::Get()->SetWorld(GetWorld());
}

// Called every frame
void ATheSimulatorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
