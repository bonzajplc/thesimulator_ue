// Fill out your copyright notice in the Description page of Project Settings.


#include "NaniteBuildTest.h"
#include "MagicaVoxelImporter.h"

#include "Math/UnrealMathUtility.h"

// Sets default values
ANaniteBuildTest::ANaniteBuildTest()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ANaniteBuildTest::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ANaniteBuildTest::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	static bool bVoxelsSet = false;

	if (!bIsMeshCreated && SMCTestActor != nullptr)
	{
		if (!bVoxelsSet)
		{
			FString voxPath = FPaths::ProjectContentDir() + "/Models/teapot.vox";
			TArray<MagicaVoxelInstance*>	Instances;
			TArray<VoxelModel>	VoxelModels;
			TArray<FVector3f> Voxels;
			EMagicaVoxelErrorCodes ErrorCode = MagicaVoxelImporter::LoadVOX(voxPath, true, Instances, VoxelModels);
			if (ErrorCode == EMagicaVoxelErrorCodes::NoError)
			{
				for (const VoxelModel Model : VoxelModels)
				{
					MagicaVoxelInstance* Instance = Instances[Model.InstanceID];
					MagicaVoxelChunk* VoxelChunk = Instance->VoxelChunk;

					for (int32 i = 0; i < Instance->VoxelChunk->NumVoxels; ++i)
					{
						FVector Position;
						Position.X = Instance->VoxelChunk->Voxels[i]->X;
						Position.Y = Instance->VoxelChunk->Voxels[i]->Y;
						Position.Z = Instance->VoxelChunk->Voxels[i]->Z;
						Position += Model.TransformPosition;
						Voxels.Add(Position);
					}
				}

			}

			/*
			const int dim = 50;
			for (int x = -dim; x < dim; ++x)
				for (int y = -dim; y < dim; ++y)
					Voxels.Add(FVector3f(x, y, FMath::Sin(x * y * 3.1415f * 0.001f) * 5.0f));
					*/

			VoxelGen.SetVoxels(Voxels);
			VoxelGen.Initialize();

			bVoxelsSet = true;
		}

		if (VoxelGen.IsMeshPrepared())
		{
			SMCTestActor->EditMesh([&](FDynamicMesh3& MeshOut)
				{
					MeshOut.Copy(&VoxelGen.Generate());
				});

			UE_LOG(LogTemp, Log, TEXT("Num triangles: %d"), VoxelGen.Triangles.Num());

			SMCTestActor->BuildNaniteData();

			bIsMeshCreated = true;
		}
	}
}

