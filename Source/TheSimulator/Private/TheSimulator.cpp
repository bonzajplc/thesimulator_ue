// Copyright Epic Games, Inc. All Rights Reserved.

#include "TheSimulator.h"
#include "Modules/ModuleManager.h"
#include "Interfaces/IPluginManager.h"

#define LOCTEXT_NAMESPACE "FTheSimulatorModule"
#include "RHICommandList.h"

void FTheSimulatorModule::StartupModule()
{
	UE_LOG(LogTemp, Log, TEXT("TheSimulator module has started!"));
	FString ShaderDirectory = FPaths::Combine(FPaths::ProjectDir(), TEXT("Shaders"));
	AddShaderSourceDirectoryMapping("/CustomShaders", ShaderDirectory);
}

void FTheSimulatorModule::ShutdownModule()
{
	ResetAllShaderSourceDirectoryMappings();
}

#undef LOCTEXT_NAMESPACE

IMPLEMENT_MODULE(FTheSimulatorModule, TheSimulator)
