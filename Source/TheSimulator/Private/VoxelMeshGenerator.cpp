#include "VoxelMeshGenerator.h"

class FGenerateVerticesCS : public FGlobalShader
{
private:

	DECLARE_GLOBAL_SHADER(FGenerateVerticesCS);
	SHADER_USE_PARAMETER_STRUCT(FGenerateVerticesCS, FGlobalShader);

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_SRV(StructuredBuffer<float3>,		Centers)
		SHADER_PARAMETER_UAV(RWStructuredBuffer<float3>,	Vertices)
		SHADER_PARAMETER_UAV(RWStructuredBuffer<float3>,	Normals)
		SHADER_PARAMETER_UAV(RWStructuredBuffer<int>,		NormalParentVertex)
		SHADER_PARAMETER(float, VoxelScale)
	END_SHADER_PARAMETER_STRUCT()

public:

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static bool ShouldCache(EShaderPlatform Platform)
	{
		return true;
	}

	static inline void
		ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters,
			FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);

		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_X"), 256);
		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_Y"), 1);
		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_Z"), 1);
	}
};

IMPLEMENT_SHADER_TYPE(, FGenerateVerticesCS, TEXT("/CustomShaders/VoxelRendering/VoxelMeshGenerator.usf"), TEXT("GenerateVertices"), SF_Compute);

class FOutputTrianglesCS : public FGlobalShader
{
private:

	DECLARE_GLOBAL_SHADER(FOutputTrianglesCS);
	SHADER_USE_PARAMETER_STRUCT(FOutputTrianglesCS, FGlobalShader);

	BEGIN_SHADER_PARAMETER_STRUCT(FParameters, )
		SHADER_PARAMETER_UAV(RWStructuredBuffer<int3>,		Triangles)
		SHADER_PARAMETER_UAV(RWStructuredBuffer<int3>,		TriangleUVs)
		SHADER_PARAMETER_UAV(RWStructuredBuffer<int3>,		TriangleNormals)
		SHADER_PARAMETER_UAV(RWStructuredBuffer<int>,		TrianglePolygonIDs)
		SHADER_PARAMETER(int, bReverseOrientation)
	END_SHADER_PARAMETER_STRUCT()

public:

	static bool ShouldCompilePermutation(const FGlobalShaderPermutationParameters& Parameters)
	{
		return IsFeatureLevelSupported(Parameters.Platform, ERHIFeatureLevel::SM5);
	}

	static bool ShouldCache(EShaderPlatform Platform)
	{
		return true;
	}

	static inline void
		ModifyCompilationEnvironment(const FGlobalShaderPermutationParameters& Parameters,
			FShaderCompilerEnvironment& OutEnvironment)
	{
		FGlobalShader::ModifyCompilationEnvironment(Parameters, OutEnvironment);

		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_X"), 256);
		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_Y"), 1);
		OutEnvironment.SetDefine(TEXT("THREADGROUPSIZE_Z"), 1);
	}
};

IMPLEMENT_SHADER_TYPE(, FOutputTrianglesCS, TEXT("/CustomShaders/VoxelRendering/VoxelMeshGenerator.usf"), TEXT("OutputEquatorialTriangles"), SF_Compute);

void VoxelMeshGenerator::Initialize()
{
	GEngine->GetDeferredRendererPrePassDelegate().AddRaw(this, &VoxelMeshGenerator::PrepareMesh);
}

void VoxelMeshGenerator::PrepareMesh(FRDGBuilder& GraphBuilder, FViewInfo& View)
{
	check(IsInRenderingThread());

	if (!bIsDirty) return;

	// enforce sane values for vertex counts
	const int32 NumVertices = 2 * 4 * Centers.Num();
	const int32 NumUVs = 8 * Centers.Num();
	const int32 NumTris = 6 * 2 * Centers.Num();
	SetBufferSizes(NumVertices, NumTris, NumUVs, NumVertices);

	{
		CentersBufferData.Empty();
		CentersBufferData.Append(Centers.GetData(), Centers.Num());
		FRHIResourceCreateInfo CentersCreateInfo(TEXT("CentersBuffer"), &CentersBufferData);
		CentersGPU.Buffer = RHICreateStructuredBuffer(
			sizeof(FVector3d),
			CentersBufferData.GetResourceDataSize(),
			BUF_Static | BUF_ShaderResource,
			CentersCreateInfo);
		CentersGPU.SRV = RHICreateShaderResourceView(CentersGPU.Buffer);
		//CentersGPU.UAV = RHICreateUnorderedAccessView(CentersGPU.Buffer, false, false);
	}

	//PrepareBuffers(&CentersBufferData, Centers.Num(), CentersGPU, TEXT("CentersBuffer"));
	PrepareBuffers(&VerticesBufferData, Vertices.Num(), VerticesGPU, TEXT("VerticesBuffer"));
	PrepareBuffers(&NormalsBufferData, Normals.Num(), NormalsGPU, TEXT("NormalsBuffer"));
	PrepareBuffers(&NormalParentVertexBufferData, NormalParentVertex.Num(), NormalParentVertexGPU, TEXT("NormalParentVertexBuffer"));

	TShaderMapRef<FGenerateVerticesCS> GenerateVerticesCS(GetGlobalShaderMap(GMaxRHIFeatureLevel));

	FGenerateVerticesCS::FParameters* GenerateVerticesParameters = GraphBuilder.AllocParameters<FGenerateVerticesCS::FParameters>();
	GenerateVerticesParameters->Centers = CentersGPU.SRV;
	GenerateVerticesParameters->Vertices = VerticesGPU.UAV;
	GenerateVerticesParameters->Normals = NormalsGPU.UAV;
	GenerateVerticesParameters->NormalParentVertex = NormalParentVertexGPU.UAV;
	GenerateVerticesParameters->VoxelScale = VoxelScale;

	FComputeShaderUtils::AddPass(
		GraphBuilder,
		RDG_EVENT_NAME("BFS Generate Vertices"),
		ERDGPassFlags::Compute |
		ERDGPassFlags::NeverCull,
		GenerateVerticesCS,
		GenerateVerticesParameters,
		FIntVector(FMath::DivideAndRoundUp(Centers.Num(), 8), 1, 1)
	);
	
	OutputTriangles(GraphBuilder, View);
	bIsDirty = false;
}

void VoxelMeshGenerator::OutputTriangles(FRDGBuilder& GraphBuilder, FViewInfo& View)
{
	PrepareBuffers(&TrianglesBufferData, Triangles.Num(), TrianglesGPU, TEXT("TrianglesBuffer"));
	PrepareBuffers(&TriangleUVsBufferData, TriangleUVs.Num(), TriangleUVsGPU, TEXT("TriangleUVsBuffer"));
	PrepareBuffers(&TriangleNormalsBufferData, TriangleNormals.Num(), TriangleNormalsGPU, TEXT("TriangleNormalsBuffer"));
	PrepareBuffers(&TrianglePolygonIDsBufferData, TrianglePolygonIDs.Num(), TrianglePolygonIDsGPU, TEXT("TrianglePolygonIDsBuffer"));

	TShaderMapRef<FOutputTrianglesCS> OutputTrianglesCS(GetGlobalShaderMap(GMaxRHIFeatureLevel));

	FOutputTrianglesCS::FParameters* OutputTrianglesParameters = GraphBuilder.AllocParameters<FOutputTrianglesCS::FParameters>();
	OutputTrianglesParameters->Triangles = TrianglesGPU.UAV;
	OutputTrianglesParameters->TriangleUVs = TriangleUVsGPU.UAV;
	OutputTrianglesParameters->TriangleNormals = TriangleNormalsGPU.UAV;
	OutputTrianglesParameters->TrianglePolygonIDs = TrianglePolygonIDsGPU.UAV;
	OutputTrianglesParameters->bReverseOrientation = bReverseOrientation ? 1 : 0;

	FComputeShaderUtils::AddPass(
		GraphBuilder,
		RDG_EVENT_NAME("Output Triangles"),
		ERDGPassFlags::Compute |
		ERDGPassFlags::NeverCull,
		OutputTrianglesCS,
		OutputTrianglesParameters,
		FIntVector(FMath::DivideAndRoundUp(Centers.Num(), 8), 1, 1)
	);

	Readback(GraphBuilder, View);
}

void VoxelMeshGenerator::Readback(FRDGBuilder& GraphBuilder, FViewInfo& View)
{
	GraphBuilder.AddPass(
		RDG_EVENT_NAME("BFS Read buffers"),
		ERDGPassFlags::None |
		ERDGPassFlags::NeverCull,
		[this](FRHICommandListImmediate& RHICmdList)
		{
			TArray<FVector3f> VerticesTemp;
			VerticesTemp.SetNum(Vertices.Num());
			ReadBuffer(VerticesGPU.Buffer, VerticesTemp.GetData());
			//Vertices.Insert(VerticesTemp.GetData(), VerticesTemp.Num(), 0);
			for (int i = 0; i < Vertices.Num(); ++i)
				Vertices[i] = FVector3d(VerticesTemp[i]); // converting from float to double

			ReadBuffer(NormalsGPU.Buffer, Normals.GetData());
			ReadBuffer(NormalParentVertexGPU.Buffer, NormalParentVertex.GetData());
			ReadBuffer(TrianglesGPU.Buffer, Triangles.GetData());
			ReadBuffer(TrianglePolygonIDsGPU.Buffer, TrianglePolygonIDs.GetData());
			ReadBuffer(TriangleUVsGPU.Buffer, TriangleUVs.GetData());
			ReadBuffer(TriangleNormalsGPU.Buffer, TriangleNormals.GetData());

			bIsMeshPrepared = true;
		}
	);
}
