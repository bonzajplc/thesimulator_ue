// Fill out your copyright notice in the Description page of Project Settings.


#include "NaniteTestActor.h"

#include "MeshComponentRuntimeUtils.h"
#include "MeshNormals.h"

// Sets default values
ANaniteTestActor::ANaniteTestActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AccumulatedTime = 0;
}

void ANaniteTestActor::PostLoad()
{
	Super::PostLoad();
	OnMeshGenerationSettingsModified();
}

void ANaniteTestActor::PostActorCreated()
{
	Super::PostActorCreated();
	OnMeshGenerationSettingsModified();
}

// Called when the game starts or when spawned
void ANaniteTestActor::BeginPlay()
{
	Super::BeginPlay();

	AccumulatedTime = 0;
	OnMeshGenerationSettingsModified();
}

// Called every frame
void ANaniteTestActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AccumulatedTime += DeltaTime;
	if (bRegenerateOnTick && SourceType == EDynamicMeshActorSourceType::Primitive)
	{
		OnMeshGenerationSettingsModified();
	}
}

#if WITH_EDITOR
void ANaniteTestActor::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);
	OnMeshGenerationSettingsModified();
}
#endif

void ANaniteTestActor::EditMesh(TFunctionRef<void(FDynamicMesh3&)> EditFunc)
{
	EditFunc(SourceMesh);

	OnMeshEditedInternal();
}

void ANaniteTestActor::GetMeshCopy(FDynamicMesh3& MeshOut)
{
	MeshOut = SourceMesh;
}

const FDynamicMesh3& ANaniteTestActor::GetMeshRef() const
{
	return SourceMesh;
}

void ANaniteTestActor::OnMeshEditedInternal()
{
	OnMeshModified.Broadcast(this);
}

void ANaniteTestActor::OnMeshGenerationSettingsModified()
{
	EditMesh([this](FDynamicMesh3& MeshToUpdate)
		{
			RegenerateSourceMesh(MeshToUpdate);
		});
}

void ANaniteTestActor::RegenerateSourceMesh(FDynamicMesh3& MeshOut)
{
	if (SourceType == EDynamicMeshActorSourceType::ImportedMesh)
	{
		MeshOut = FDynamicMesh3();
	}

	RecomputeNormals(MeshOut);
}

void ANaniteTestActor::RecomputeNormals(FDynamicMesh3& MeshOut)
{
	if (this->NormalsMode == EDynamicMeshActorNormalsMode::PerVertexNormals)
	{
		MeshOut.EnableAttributes();
		FMeshNormals::InitializeOverlayToPerVertexNormals(MeshOut.Attributes()->PrimaryNormals(), false);
	}
	else if (this->NormalsMode == EDynamicMeshActorNormalsMode::FaceNormals)
	{
		MeshOut.EnableAttributes();
		FMeshNormals::InitializeOverlayToPerTriangleNormals(MeshOut.Attributes()->PrimaryNormals());
	}
}

int ANaniteTestActor::GetTriangleCount()
{
	return SourceMesh.TriangleCount();
}

ADynamicSMCActor::ADynamicSMCActor()
{
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"), false);
	SetRootComponent(MeshComponent);
	StaticMesh = nullptr;
}

void ADynamicSMCActor::BeginPlay()
{
	StaticMesh = nullptr;
	Super::BeginPlay();
}

void ADynamicSMCActor::PostLoad()
{
	StaticMesh = nullptr;
	Super::PostLoad();
}

void ADynamicSMCActor::PostActorCreated()
{
	StaticMesh = nullptr;
	Super::PostActorCreated();
}

void ADynamicSMCActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void ADynamicSMCActor::OnMeshEditedInternal()
{
	UpdateSMCMesh();
	Super::OnMeshEditedInternal();
}

void ADynamicSMCActor::BuildNaniteData()
{
	FMeshNaniteSettings NaniteSettings;
	NaniteSettings.bEnabled = true;
	NaniteSettings.PercentTriangles = 100.0f;
	NaniteSettings.PositionPrecision = 100;
	MeshComponent->GetStaticMesh()->NaniteSettings = NaniteSettings;
	MeshComponent->GetStaticMesh()->Build();
}

void ADynamicSMCActor::UpdateSMCMesh()
{
	if (StaticMesh == nullptr)
	{
		StaticMesh = NewObject<UStaticMesh>();
		MeshComponent->SetStaticMesh(StaticMesh);
		StaticMesh->SetStaticMaterials({ FStaticMaterial() });
	}

	if (MeshComponent)
	{
		RTGUtils::UpdateStaticMeshFromDynamicMesh(StaticMesh, &SourceMesh);

		UMaterialInterface* UseMaterial = (this->Material != nullptr) ? this->Material : UMaterial::GetDefaultMaterial(MD_Surface);
		MeshComponent->SetMaterial(0, UseMaterial);
	}
}
