#pragma once

#include "RHICommandList.h"

struct GPUData
{
	FBufferRHIRef Buffer = nullptr;
	FShaderResourceViewRHIRef SRV = nullptr;
	FUnorderedAccessViewRHIRef UAV = nullptr;
};

template<typename T>
void PrepareBuffers(TResourceArray<T>* BufferData, int NumElements, GPUData& GPUdata, TCHAR* BufferName)
{
	BufferData->Empty();
	BufferData->SetNum(NumElements);
	BufferData->SetAllowCPUAccess(true);
	FRHIResourceCreateInfo CreateInfo(BufferName, BufferData);
	GPUdata.Buffer = RHICreateStructuredBuffer(
		sizeof(T),
		BufferData->GetResourceDataSize(),
		BUF_Static | BUF_UnorderedAccess,
		CreateInfo);
	//GPUdata.SRV = RHICreateShaderResourceView(GPUdata.Buffer);
	GPUdata.UAV = RHICreateUnorderedAccessView(GPUdata.Buffer, false, false);
}

template<typename T>
void ReadBuffer(FBufferRHIRef Buffer, T* Dest)
{
	const void* Data = RHILockBuffer(Buffer, 0, Buffer->GetSize(), RLM_ReadOnly);
	FMemory::Memcpy(Dest, Data, Buffer->GetSize());
	RHIUnlockBuffer(Buffer);
}
