// Fill out your copyright notice in the Description page of Project Settings.


#include "ConsoleCommandsComponent.h"

bool UConsoleCommandsComponent::bSetUnlimitedFPS = false;
bool UConsoleCommandsComponent::bShownFPS = false;
bool UConsoleCommandsComponent::bShownFrameBudget = false;
bool UConsoleCommandsComponent::bShownGPUStats = false;
bool UConsoleCommandsComponent::bShownNaniteStats = false;

// Sets default values for this component's properties
UConsoleCommandsComponent::UConsoleCommandsComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UConsoleCommandsComponent::BeginPlay()
{
	Super::BeginPlay();

	if (bShowFPS && !bShownFPS)
	{
		GEngine->Exec(GetWorld(), TEXT("stat FPS")); // Show FPS stats
		bShownFPS = true;
	}
	else if (!bShowFPS && bShownFPS)
	{
		GEngine->Exec(GetWorld(), TEXT("stat FPS")); // Show FPS stats
		bShownFPS = false;
	}
	if (bShowFrameBudget && !bShownFrameBudget)
	{
		GEngine->Exec(GetWorld(), TEXT("stat UnitGraph")); // Show frame budget
		bShownFrameBudget = true;
	}
	else if (!bShowFrameBudget && bShownFrameBudget)
	{
		GEngine->Exec(GetWorld(), TEXT("stat UnitGraph")); // Show frame budget
		bShownFrameBudget = false;
	}
	if (bShowGPUStats && !bShownGPUStats)
	{
		GEngine->Exec(GetWorld(), TEXT("stat RHI")); // Show GPU memory and draw calls
		bShownGPUStats = true;
	}
	else if (!bShowGPUStats && bShownGPUStats)
	{
		GEngine->Exec(GetWorld(), TEXT("stat RHI")); // Show GPU memory and draw calls
		bShownGPUStats = false;
	}
	if (bShowNaniteStats && !bShownNaniteStats)
	{
		GEngine->Exec(GetWorld(), TEXT("Nanitestats")); // Show Nanite stats
		bShownNaniteStats = true;
	}
	else if (!bShowNaniteStats && bShownNaniteStats)
	{
		GEngine->Exec(GetWorld(), TEXT("Nanitestats")); // Show Nanite stats
		bShownNaniteStats = false;
	}
	if (bUnlimitedFPS && !bSetUnlimitedFPS)
	{
		GEngine->Exec(GetWorld(), TEXT("t.MaxFPS 1000")); // Unlimited FPS
		bSetUnlimitedFPS = true;
	}
	else if (!bUnlimitedFPS && bSetUnlimitedFPS)
	{
		GEngine->Exec(GetWorld(), TEXT("t.MaxFPS 120")); // Limited FPS
		bSetUnlimitedFPS = false;
	}
	// ...
	
}


// Called every frame
void UConsoleCommandsComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

